# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import pathlib
import sys
pv = sys.version_info
if pv[0] >= 3 and pv[1] >= 8:
    ["pandas", "numpy", "matplotlib", "scipy", "plotly"]
else:
    ["pandas", "numpy", "matplotlib", "scipy", "plotly", "pickle5"]
    
here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name="hwlhw", # Replace with your own username
    version="1.0rc7",
    author="Ruben Müller, Büro für Angewandte Hydrologie",
    author_email="software@bah-berlin.de",
    description="Statistical method for the generation of synthetic flood events.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/BAH_Berlin/hwlhw",
    packages=find_packages(where="src"), 
    package_dir={'': 'src'}, 
    license = "gpl",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5, <4',
    setup_requires=["numpydoc", "wheel"],
    extras_require={'notebook': ["notebook", "ipywidgets", "ipympl"]},
    install_requires=["pandas", "numpy", "matplotlib", "scipy", "plotly"]
)
