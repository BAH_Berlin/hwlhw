# -*- coding: utf-8 -*-
"""
Functions using ipywidgets for providing interactive widgets in Jupyter notebooks.

Created on Wed May  5 09:59:14 2021

@author: ruben.mueller
"""
import os
import pickle
try:
    import ipywidgets as widgets
except:
    print("Warning: ipywidgets is not installed or could not be found!" )
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from hwlhw.hwlhw_settings import TB_MODELMAP
from hwlhw.hwlhw_utils import plot_scatter


class Select_hqs_mean:
    """Class for the widgets for the distribution and estimator selection"""
    def __init__(self, dist_param):
        self.dist_param = dist_param
        self.list_o = {'season_0': {'HQ*': ['E1', 'MM'],
                                    'HQ1': ['E1', 'MM'], 
                                    'HQ2': ['E1', 'MM'],
                                    'HQ3': ['E1', 'MM']},
                       'season_1': {'HQ*': ['E1', 'MM'],
                                    'HQ1': ['E1', 'MM'], 
                                    'HQ2': ['E1', 'MM'],
                                    'HQ3': ['E1', 'MM']}}
        self.hqT = '100'
        self.sims = {}

        x = 'season_0'
        for i, hq in enumerate(self.list_o[x]):
            if not hq == 'HQ*':
                self.list_o[x][hq] = list(dist_param.modelvar_fit[x]['p']['q_s'].index[i+1])
        
        self.list_o[x]['HQ*'] = list(dist_param.modelvar_fit[x]['p']['q_s'].index[0])            
        if len([x for x in self.dist_param.parameter_season]) == 2:
            x = 'season_1'
            for i, hq in enumerate(self.list_o[x]):
                if not hq == 'HQ*':
                    self.list_o[x][hq] = list(dist_param.modelvar_fit[x]['p']['q_s'].index[i+1])

            self.list_o[x]['HQ*'] = list(dist_param.modelvar_fit[x]['p']['q_s'].index[0]) 


    def show(self, hqsim, m_fit, display):

        dopt = ['E1', 'AE', 'LN3', 'P3', 'LP3', 'WB3', 'ROV']
        doptd = {'E1':'1', 'AE':'2', 'LN3':'3', 'P3':'4', 'LP3':'5', 'WB3':'6', 'ROV':'7'}
        dest = ['MM', 'MLM', 'WGM']
        destd = {'MM':'1', 'MLM':'2', 'WGM':'3'}
        
        dhq = ['2', '5', '10', '20', '25', '50', '100', '200', '500', '1000']
        dhqd = {'2':'1', '5':'2', '10':'3', 
                '20':'4', '25':'5', '50':'6', 
                '100':'7', '200':'8', '500':'9', '1000':'10'}
        

        if len([x for x in self.dist_param.parameter_season]) == 1:
            opts_seas = ['season_0']
        else:
            opts_seas = ['season_0', 'season_1']

        widget_HQT = widgets.Dropdown(
            options=dhqd,
            description='Basis HQT',
            disabled=False)
        
        # ---- seas 0
        widget_s0based = widgets.Dropdown(
            options=doptd,
            description='HQB Verteilung',
            disabled=False)
        widget_s0basee = widgets.Dropdown(
            options=destd,
            description='HQB Schätzer',
            disabled=False)
        widget_s0q0d = widgets.Dropdown(
            options=doptd,
            description='HQ1 Verteilung',
            disabled=False)
        widget_s0q0e = widgets.Dropdown(
            options=destd,
            description='HQ1 Schätzer',
            disabled=False) 
        widget_s0q1d = widgets.Dropdown(
            options=doptd,
            description='HQ2 Verteilung',
            disabled=False)
        widget_s0q1e = widgets.Dropdown(
            options=destd,
            description='HQ2 Schätzer',
            disabled=False)
        widget_s0q2d = widgets.Dropdown(
            options=doptd,
            description='HQ3 Verteilung',
            disabled=False)
        widget_s0q2e = widgets.Dropdown(
            options=destd,
            description='HQ3 Schätzer',
            disabled=False)
        
        # ---- seas 1
        if len(opts_seas) == 2:
            widget_s1based = widgets.Dropdown(
                options=doptd,
                description='HQB Verteilung',
                disabled=False)
            widget_s1basee = widgets.Dropdown(
                options=doptd,
                description='HQB Schätzer',
                disabled=False)
            widget_s1q0d = widgets.Dropdown(
                options=doptd,
                description='HQ1 Verteilung',
                disabled=False)
            widget_s1q0e = widgets.Dropdown(
                options=destd,
                description='HQ1 Schätzer',
                disabled=False) 
            widget_s1q1d = widgets.Dropdown(
                options=doptd,
                description='HQ2 Verteilung',
                disabled=False)
            widget_s1q1e = widgets.Dropdown(
                options=destd,
                description='HQ2 Schätzer',
                disabled=False)
            widget_s1q2d = widgets.Dropdown(
                options=doptd,
                description='HQ3 Verteilung',
                disabled=False)
            widget_s1q2e = widgets.Dropdown(
                options=destd,
                description='HQ3 Schätzer',
                disabled=False)

        widget_accept = widgets.Button(description="Übernehmen und Simulieren")

            
        tmp = dhq.index(self.hqT)
        widget_HQT.value = str(tmp+1)
        #print(tmp, str(tmp+1))

        tmp = dopt.index(self.list_o['season_0']['HQ*'][0])
        widget_s0based.value = str(tmp+1)
        tmp = dest.index(self.list_o['season_0']['HQ*'][1])
        widget_s0basee.value = str(tmp+1)

        tmp = dopt.index(self.list_o['season_0']['HQ1'][0])
        widget_s0q0d.value = str(tmp+1)
        tmp = dest.index(self.list_o['season_0']['HQ1'][1])
        widget_s0q0e.value = str(tmp+1)
        tmp = dopt.index(self.list_o['season_0']['HQ2'][0])
        widget_s0q1d.value = str(tmp+1)
        tmp = dest.index(self.list_o['season_0']['HQ2'][1])
        widget_s0q1e.value = str(tmp+1)
        tmp = dopt.index(self.list_o['season_0']['HQ3'][0])
        widget_s0q2d.value = str(tmp+1)
        tmp = dest.index(self.list_o['season_0']['HQ3'][1])
        widget_s0q2e.value = str(tmp+1)

        if len(opts_seas) == 2:
            tmp = dopt.index(self.list_o['season_1']['HQ*'][0])
            widget_s1based.value = str(tmp+1)
            tmp = dest.index(self.list_o['season_1']['HQ*'][1])
            widget_s1basee.value = str(tmp+1)

            tmp = dopt.index(self.list_o['season_1']['HQ1'][0])
            widget_s1q0d.value = str(tmp+1)
            tmp = dest.index(self.list_o['season_1']['HQ1'][1])
            widget_s1q0e.value = str(tmp+1)
            tmp = dopt.index(self.list_o['season_1']['HQ2'][0])
            widget_s1q1d.value = str(tmp+1)
            tmp = dest.index(self.list_o['season_1']['HQ2'][1])
            widget_s1q1e.value = str(tmp+1)
            tmp = dopt.index(self.list_o['season_1']['HQ3'][0])
            widget_s1q2d.value = str(tmp+1)
            tmp = dest.index(self.list_o['season_1']['HQ3'][1])
            widget_s1q2e.value = str(tmp+1) 
            
        def select(l1, l2, lst):
            return l1[int(lst[0])-1], l2[int(lst[1])-1]

        # ---------------------------
        def on_widget_accept(c):

            x = self.dist_param.stx_dist['season_0']['p']['q_s'].df_stx
            self.hqT = dhq[int(widget_HQT.value)-1]
            hqt_val = 'HQ({})'.format(self.hqT)
            self.list_o['season_0'].update({'HQ*': [widget_s0based.value, widget_s0basee.value]})
            self.list_o['season_0'].update({'HQ1': [widget_s0q0d.value, widget_s0q0e.value]}) 
            self.list_o['season_0'].update({'HQ2': [widget_s0q1d.value, widget_s0q1e.value]}) 
            self.list_o['season_0'].update({'HQ3': [widget_s0q2d.value, widget_s0q2e.value]})

            self.sims = {'season_0': {'HQ*': x.loc[select(dopt, dest, self.list_o['season_0']['HQ*']), hqt_val],
                         'HQ1': x.loc[select(dopt, dest, self.list_o['season_0']['HQ1']), hqt_val],
                         'HQ2': x.loc[select(dopt, dest, self.list_o['season_0']['HQ2']), hqt_val],
                         'HQ3': x.loc[select(dopt, dest, self.list_o['season_0']['HQ3']), hqt_val],
                        }}
            
            if len(opts_seas) == 2:
                x = self.dist_param.stx_dist['season_1']['p']['q_s'].df_stx
                self.list_o['season_1'].update({'HQ*': [widget_s1based.value, widget_s1basee.value]})
                self.list_o['season_1'].update({'HQ1': [widget_s1q0d.value, widget_s1q0e.value]}) 
                self.list_o['season_1'].update({'HQ2': [widget_s1q1d.value, widget_s1q1e.value]}) 
                self.list_o['season_1'].update({'HQ3': [widget_s1q2d.value, widget_s1q2e.value]}) 
            
                self.sims.update({'season_1': {'HQ*': x.loc[select(dopt, dest, self.list_o['season_1']['HQ*']), hqt_val],
                         'HQ1': x.loc[select(dopt, dest, self.list_o['season_1']['HQ1']), hqt_val],
                         'HQ2': x.loc[select(dopt, dest, self.list_o['season_1']['HQ2']), hqt_val],
                         'HQ3': x.loc[select(dopt, dest, self.list_o['season_1']['HQ3']), hqt_val],
                        }})

            hqsim.mean_event_method(m_fit, self.sims, dp_factor=10)
            show_results_mean(hqsim, display)
        # ---------------------------
        
        # SHOW wigdet
        vboxlist = [widgets.HBox([widget_HQT]),
                      widgets.HBox([widgets.Label('--- Saison 0 ---')]),
                      widgets.HBox([widget_s0based, widget_s0basee]),
                      widgets.HBox([widget_s0q0d, widget_s0q0e]),
                      widgets.HBox([widget_s0q1d, widget_s0q1e]),
                      widgets.HBox([widget_s0q2d, widget_s0q2e])]
        if len(opts_seas) == 2:
           vboxlist += [widgets.HBox([widgets.Label('--- Saison 1 ---')]),
                      widgets.HBox([widget_s1based, widget_s0basee]),
                      widgets.HBox([widget_s1q0d, widget_s1q0e]),
                      widgets.HBox([widget_s1q1d, widget_s1q1e]),
                      widgets.HBox([widget_s1q2d, widget_s1q2e])]

        vboxlist += [widgets.HBox([widget_accept])]
        display(widgets.VBox(vboxlist))        

        widget_accept.on_click(on_widget_accept)
        


    def save_state(self, workspace):
        with open(os.path.join(workspace, 'Select_hqt_mean_settings.pkl'), 'wb') as fid:
            pickle.dump([self.list_o, self.hqT], fid)

    @staticmethod
    def load_state(workspace, dist_param):
        with open(os.path.join(workspace, 'Select_hqt_mean_settings.pkl'), 'rb') as fid:
            tmp = pickle.load(fid)
            x = Select_hqs_mean(dist_param)
            x.list_o = tmp[0]
            x.hqT = tmp[1]
            return x



def show_scatter_plot(hqsim, m_fit, display):
    """Show the scatter plot between two chosen variables."""
    widget_out_evnt = widgets.Output()

    widget_model = widgets.Dropdown(
        options= ['Monte-Carlo', 'Mittlere Methode'],
        description='Seasons',
        disabled=False)

    widget_x = widgets.Dropdown(
        options=['t_a', 'vol', 't_p', 'q_s', 'm_an', 'm_ab'],
        description='X-Achse',
        disabled=False)

    widget_y = widgets.Dropdown(
        options=['vol', 't_a', 't_p', 'q_s', 'm_an', 'm_ab'],
        description='Y-Achse',
        disabled=False)

    widget_ok = widgets.Button(description="Darstellen")

    def on_widget_accept(c):
        #season = widget_season.value
        parx = widget_x.value
        pary = widget_y.value
        mc = widget_model.value

        mc_mean = 'mean' if mc == 'Mittlere Methode' else 'mc'
        with widget_out_evnt:
            widget_out_evnt.clear_output()
            plot_scatter(parx, pary, hqsim, m_fit, mc_mean=mc_mean)

    widget_ok.on_click(on_widget_accept)
    display(widgets.VBox([widgets.HBox([widget_model]),
                          widgets.HBox([widget_x, widget_y]),
                          widget_ok, widget_out_evnt]))



def show_results_mean(hqsim, display=None, ret_text=None, hq_events_m=None):
    """Print some information about the simulated floods"""
    if not display is None:
        out = widgets.Output(layout={'border': '1px solid black'})
        display(out)
    if hq_events_m is None:
        hq_events_m = hqsim.mean_m_p
        
    x = ""
    for seas in hq_events_m:
        x += seas + "\n"
        x += '='*len(seas) + "\n"+ "\n"
        for qs in hq_events_m[seas]:
            x += 'Ereignis Q{}'.format(qs) + "\n"
            x += '-'*len('Ereignis Q{}'.format(qs)) + "\n"
            x += '    {}'.format(hq_events_m[seas][qs])
            x +=  "\n"+ "\n"
        x +=  "\n"
        if ret_text is None and not display is None:
            with out:
                for seas in hq_events_m:
                    print(x)
    if ret_text is not None:
        return x
            


class Select_dist_est:
    """Class for the widgets for the distribution and estimator selection"""
    def __init__(self, dist_param):
        self.list_o = {'season_0': {'m_an': ['E1', 'MM'], 'm_ab': ['E1', 'MM']
                                    ,'q_s': ['E1', 'MM'],
                                    't_p': ['E1', 'MM'], 't_a': ['E1', 'MM']},
                       'season_1': {'m_an': ['E1', 'MM'], 'm_ab': ['E1', 'MM'],
                                    'q_s': ['E1', 'MM'],
                                    't_p': ['E1', 'MM'], 't_a': ['E1', 'MM']}}
        for x in dist_param.modelvar_fit:
            for p in dist_param.modelvar_fit[x]['p']:
                if p == 'modelname':
                    continue
                self.list_o[x][p] = list(dist_param.modelvar_fit[x]['p'][p].index[0])

        self.dist_param = dist_param

    def show(self, display):
        dpar = ['t_a', 'm_an', 'm_ab', 'q_s', 't_p']
        dopt = ['E1', 'AE', 'LN3', 'P3', 'LP3', 'WB3', 'ROV']
        doptd = {'E1':'1', 'AE':'2', 'LN3':'3', 'P3':'4', 'LP3':'5', 'WB3':'6', 'ROV':'7'}
        dest = ['MM', 'MLM', 'WGM']
        destd = {'MM':'1', 'MLM':'2', 'WGM':'3'}
    
        widget_out_1 = widgets.Output()
        widget_out_2 = widgets.Output()

        if len([x for x in self.dist_param.parameter_season]) == 1:
            opts_seas = ['season_0']
        else:
            opts_seas = ['season_0', 'season_1']

        widget_season = widgets.Dropdown(
            options=opts_seas,
            description='Seasons',
            disabled=False)

        widget_param = widgets.Dropdown(
            options=['t_a', 'm_an', 'm_ab', 'q_s', 't_p'],
            description='Parameter',
            disabled=False)

        widget_dist = widgets.Dropdown(
            options=[],
            description='Verteilung',
            disabled=False)

        widget_est = widgets.Dropdown(
            options=[],
            description='Schätzer',
            disabled=False)

        widget_show = widgets.Button(description="Anzeigen")
        widget_accept = widgets.Button(description="Übernehmen")

        def on_widget_show(c):
            season = widget_season.value
            param = widget_param.value
            x = self.dist_param.modelvar_dist[season]['p'][param].df_prx
            y = self.dist_param.modelvar_fit[season]['p'][param]
            y = y.to_frame()
            y.columns = ['Goodness of fit']
            #print(y.columns)
            #print(type(x), type(y))
            val_d = dopt.index(self.list_o[season][param][0])
            if not param == 'ROV':
                val_p = dest.index(self.list_o[season][param][1])
            else:
                val_p = '0'
            #print(self.list_o[season][param], val_d, val_p)

            widget_dist.options = {}
            
            widget_dist.options = doptd
            widget_dist.value = str(val_d+1)
            
            widget_est.options = destd
            widget_est.value = str(val_p+1)

            with widget_out_1:
                widget_out_1.clear_output()
                display(x.iloc[:,-3:])

            with widget_out_2:
                widget_out_2.clear_output()
                display(y)


        def on_widget_accept(c):
            season = widget_season.value
            para = widget_param.value
            dx = dopt[int(widget_dist.value)-1]
            px = dest[int(widget_est.value)-1]

            self.list_o[season].update({para: [dx, px]}) 
            #print(self.list_o[season])

        display(widgets.VBox([widgets.HBox([widget_season, widget_param, widget_show]),
                              widgets.HBox([widget_dist, widget_est, widget_accept]),
                              widgets.HBox([widget_out_1, widget_out_2])]))        


        widget_show.on_click(on_widget_show)
        widget_accept.on_click(on_widget_accept)

    def save_state(self, workspace):
        with open(os.path.join(workspace, 'Select_dist_est_settings.pkl'), 'wb') as fid:
            pickle.dump(self.list_o, fid)

    @staticmethod
    def load_state(workspace, dist_param):
        with open(os.path.join(workspace, 'Select_dist_est_settings.pkl'), 'rb') as fid:
            tmp = pickle.load(fid)
            x = Select_dist_est(dist_param)
            x.list_o = tmp
            return x


class Config_save_load():
    """Class shows the load_project and save_variable widget and tracks changes"""
    def __init__(self):
        self.project_reload = True
        self.save_it = True
        #self.show()

    def sig_rl(self, change):
        self.project_reload = change['new']
    def sig_sv(self, change):
        self.save_it = change['new']

    def show(self, display):
        widget_rl = widgets.Checkbox(
            value=True,
            description='Projekt laden',
            disabled=False
        )

        widget_sv = widgets.Checkbox(
            value=True,
            description='Speichern nach ausführen',
            disabled=False
        )
        widget_rl.observe(self.sig_rl, names='value')
        widget_sv.observe(self.sig_sv, names='value')

        display(widgets.HBox([widget_rl, widget_sv]))


def print_event_info(m_fit, option, event=None):
    """Print observed and simulated parameters for a event"""
    modelname = m_fit.s_ev.events[option]['modelname']
    if 't_a' in TB_MODELMAP[modelname]['parameter']:
        idx = TB_MODELMAP[modelname]['parameter'].index('t_a')
        t_a = m_fit.s_ev.events[option]['parameter'][idx]
    else:
        idx = TB_MODELMAP[modelname]['parameter_fixed'].index('t_a')
        t_a = m_fit.s_ev.events[option]['parameter_fixed'][idx]
    if 'q_s' in TB_MODELMAP[modelname]['parameter']:
        idx = TB_MODELMAP[modelname]['parameter'].index('q_s')
        q_s = m_fit.s_ev.events[option]['parameter'][idx]
    else:
        idx = TB_MODELMAP[modelname]['parameter_fixed'].index('q_s')
        q_s = m_fit.s_ev.events[option]['parameter_fixed'][idx]
        


    x = ("""Beobachtetes Volumen:\t{:.2f}
Simuliertes Volumen:\t{:.2f}
Gütekriterium:\t{}\t{:.2f}

Scheiteldurchfluss (beob. oder opt.):\t{:.2f}
Basisabfluss:\t{:.2f}
t_a Zeitschritte (beob. oder opt.):\t{:.2f}
""".format(m_fit.s_ev.events[option]['vol_obs'],
        m_fit.s_ev.events[option]['vol_sim'],
        m_fit.s_ev.events[option]['fitnesstype'],
        m_fit.s_ev.events[option]['fitness'], ####
        q_s,
        m_fit.s_ev.events[option]['Qbase'],
        t_a,
        ))
    x += ("\n\nParameter:\t{}\nModelname:\t{}\n".format(['{:.3f}'.format(x) for x in m_fit.s_ev.events[option]['parameter']],
        modelname))
    
    if event is None:
        print(x)
    else:
        x = event + "\n---------------------------\n" + x
        return x
        


class Show_hq_bf:
    """Show flood events by clicking forward and backward"""
    def __init__(self, s_ev, display):

        widget_out_evnt = widgets.Output()

        widget_forward = widgets.Button(description=">")
        widget_backward = widgets.Button(description="<")
        widget_forward0 = widgets.Button(description=">>")
        widget_backward0 = widgets.Button(description="<<")

        self.eventlist = [x for x in s_ev.events]
        self.lenx = len(self.eventlist)
        self.current_state = 0

        def on_widget_forward(args):
            self.current_state += 1
            self.current_state = 0 if self.current_state >= self.lenx else self.current_state

            on_widget()

        def on_widget_backward(args):
            self.current_state -= 1
            self.current_state = self.lenx if self.current_state == -1 else self.current_state
            on_widget()

        def on_widget_forward0(args):
            self.current_state += 10
            self.current_state = 0 if self.current_state >= self.lenx else self.current_state
            on_widget()

        def on_widget_backward0(args):
            self.current_state -= 10
            self.current_state = self.lenx if self.current_state < 0 else self.current_state
            on_widget()

        def on_widget():
            if self.current_state >= len(self.eventlist):
                return
            event = s_ev.events[self.eventlist[self.current_state]]
            with widget_out_evnt:
                widget_out_evnt.clear_output()
                fig = px.line(s_ev.timeseries.series_gauge.loc[event["start"]:event["end"]], y="Q",
                              title='{} / {} -- {}'.format(self.current_state+1, self.lenx, self.eventlist[self.current_state]))
                fig.show()

        widget_forward.on_click(on_widget_forward)
        widget_forward0.on_click(on_widget_forward0)
        widget_backward.on_click(on_widget_backward)
        widget_backward0.on_click(on_widget_backward0)

        display(widgets.VBox([widgets.HBox([widget_backward0, widget_backward, widget_forward, widget_forward0]),
                             widget_out_evnt]))


def show_hq_bf(s_ev, display):
    Show_hq_bf(s_ev, display)


def show_hq_events(s_ev, display):
    """Show figure of selected event.

    Two Drop down menues for selection of season and event.

    Parameters
    ----------
    s_ev : hwlhw.Separate_events
        Class with selected events
    display : ipywidgets.display
        the display driver"""
    widget_out_evnt = widgets.Output()

    if len(s_ev.seasons) == 1:
        opts_seas = ['Auswahl'] + ['Season_0']
    else:
        opts_seas = ['Auswahl'] + ['Season_0', 'Season_1']
    widget_season = widgets.Dropdown(
        options= opts_seas,
        description='Seasons',
        disabled=False)

    widget_dd_hsep = widgets.Dropdown(
        options= ['Auswahl'],
        description='Ereignisse',
        disabled=False)

    def on_widget_season(c):

        ddval = lambda c: c['new'] if c['type'] == 'change' and c['name'] == 'value' else None
        option = ddval(c)

        if not option is None and not option == "Auswahl":
            seas = int(option[-1])
            #print(seas)
            widget_dd_hsep.index = None
            widget_dd_hsep.options = ['Auswahl'] + [x[0] for x in s_ev.events.items() if x[1]['season'] == seas]

    def on_widget_dd_hsep(c):
        ddval = lambda c: c['new'] if c['type'] == 'change' and c['name'] == 'value' else None
        option = ddval(c)
        if not option is None and not option == "Auswahl":
            event = s_ev.events[option]
            with widget_out_evnt:
                widget_out_evnt.clear_output()
                fig = px.line(s_ev.timeseries.series_gauge.loc[event["start"]:event["end"]], y="Q",
                              title=option)
                fig.show()

    widget_season.observe(on_widget_season)
    widget_dd_hsep.observe(on_widget_dd_hsep)

    display(widgets.VBox([widgets.HBox([widget_season, widget_dd_hsep]),
                         widget_out_evnt]))


def single_event_fitting(m_fit, display):
    """Fit the model to a single event.

    Two Drop down menues for selection of season and event.
    Button to beginn the fitting process.

    Parameters
    ----------
    m_fit : hwlhw.Model_fitter
        Class for fitting models
    display : ipywidgets.display
        the display driver"""
    if len(m_fit.s_ev.seasons) == 1:
        opts_seas = ['Auswahl'] + ['Season_0']
    else:
        opts_seas = ['Auswahl'] + ['Season_0', 'Season_1']
    widget_season = widgets.Dropdown(
        options= opts_seas,
        description='Seasons',
        disabled=False)

    widget_dd_hsep = widgets.Dropdown(
        options= ['Auswahl'],
        description='Ereignisse',
        disabled=False)

    widget_dd_but = widgets.Button(description="Anpassen")
    widget_out_evnt = widgets.Output()

    widget_out_evnt_txt = widgets.Output()

    def on_widget_season(c):

        ddval = lambda c: c['new'] if c['type'] == 'change' and c['name'] == 'value' else None
        option = ddval(c)

        if not option is None and not option == "Auswahl":
            seas = int(option[-1])
            widget_dd_hsep.index = None
            widget_dd_hsep.options = ['Auswahl'] + [x[0] for x in m_fit.s_ev.events.items() if x[1]['season'] == seas]

    def on_widget_button(args):
        option = widget_dd_hsep.value
        m_fit.optimize_single_event(option)
        event = m_fit.s_ev.events[option]
        with widget_out_evnt:
            widget_out_evnt.clear_output()
            fig = make_subplots(rows=1, cols=1)
            fig.add_trace(go.Scatter(y=m_fit.s_ev.timeseries.series_gauge.loc[event["start"]:event["end"], "Q"],
                                     x=m_fit.s_ev.timeseries.series_gauge.loc[event["start"]:event["end"], "Q"].index,
                                     mode="lines", name="obs"),
                          row=1, col=1)
            fig.add_trace(go.Scatter(y=m_fit.s_ev.events[option]['timeseries'].loc[:,0],
                                     x=m_fit.s_ev.timeseries.series_gauge.loc[event["start"]:event["end"], "Q"].index,
                                     mode="lines", name="fit"),
                          row=1, col=1)
            fig.show()

        with widget_out_evnt_txt:
            widget_out_evnt_txt.clear_output()
            print_event_info(m_fit, option)

    widget_season.observe(on_widget_season)
    widget_dd_but.on_click(on_widget_button)
    display(widgets.VBox([widgets.HBox([widget_season, widget_dd_hsep, widget_dd_but]),
                          widget_out_evnt, widget_out_evnt_txt]))


def show_param_fit(hqex_results, display):
    """Show widget for imported HQ-EX fitted parameter and HQ(T) results.

    Parameters
    ----------
    hqex_results : Hqex_dist_para object
        """
    widget_out_param = widgets.Output()
    widget_out_hqthx = widgets.Output()
    display(widgets.HBox([widget_out_param, widget_out_hqthx]))
    with widget_out_param:
        widget_out_param.clear_output()
        display(hqex_results.df_prx)
    with widget_out_hqthx:
        widget_out_hqthx.clear_output()
        display(hqex_results.df_stx)
    hqex_results.widget_out_param = widget_out_param
    hqex_results.widget_out_hqthx = widget_out_hqthx


def show_select_dist(hqex_results, display):
    widget_dd_hd = widgets.Dropdown(options=hqex_results.get_distributions())
    display(widget_dd_hd)
    hqex_results.widget_dd_hd = widget_dd_hd


def show_hq_year_hydyear(ts_q, display):
    widget_out_jhq = widgets.Output()

    widget_dd_jhq = widgets.Dropdown(
        options= ["Kaldenderjahr", "hydrologisches Jahr"],
        value= "hydrologisches Jahr",
        description='HQ nach',
        disabled=False,
    )
    def on_widget_pb_jhq(args):
        with widget_out_jhq:
            widget_out_jhq.clear_output()
            if widget_dd_jhq.value == "Kaldenderjahr":
                display(ts_q.df_hq_year)
            else:
                display(ts_q.df_hq_hydyear)

    widget_pb_jhq = widgets.Button(description="Anzeigen")
    widget_pb_jhq.on_click(on_widget_pb_jhq)
    display(widgets.HBox([widget_dd_jhq, widget_pb_jhq]))
    display(widget_out_jhq)
    ts_q.widget_pb_jhq = widget_pb_jhq



def show_fitted_event(m_fit, display):
    """Show figure of selected event and the respective fitted modell.

    Two Drop down menues for selection of season and event.

    Parameters
    ----------
    m_fit : hwlhw.Model_fitter
        Class for fitting models
    display : ipywidgets.display
        the display driver"""
    widget_out_evnt = widgets.Output()

    if len(m_fit.s_ev.seasons) == 1:
        opts_seas = ['Auswahl'] + ['Season_0']
    else:
        opts_seas = ['Auswahl'] + ['Season_0', 'Season_1']
    widget_season = widgets.Dropdown(
        options= opts_seas,
        description='Seasons',
        disabled=False)

    widget_dd_hsep = widgets.Dropdown(
        options= ['Auswahl'],
        description='Ereignisse',
        disabled=False)

    widget_out_evnt_txt = widgets.Output()

    def on_widget_season(c):

        ddval = lambda c: c['new'] if c['type'] == 'change' and c['name'] == 'value' else None
        option = ddval(c)

        if not option is None and not option == "Auswahl":
            seas = int(option[-1])
            #print(seas)
            widget_dd_hsep.index = None
            widget_dd_hsep.options = ['Auswahl'] + [x[0] for x in m_fit.s_ev.events.items() if x[1]['season'] == seas]

    def on_widget_dd_hsep(c):
        ddval = lambda c: c['new'] if c['type'] == 'change' and c['name'] == 'value' else None
        option = ddval(c)
        if not option is None and not option == "Auswahl":
            event = m_fit.s_ev.events[option]
            with widget_out_evnt:
                widget_out_evnt.clear_output()
                fig = make_subplots(rows=1, cols=1)
                fig.add_trace(go.Scatter(y=m_fit.s_ev.timeseries.series_gauge.loc[event["start"]:event["end"], "Q"],
                                         x=m_fit.s_ev.timeseries.series_gauge.loc[event["start"]:event["end"], "Q"].index,
                                         mode="lines", name="obs"),
                              row=1, col=1)
                fig.add_trace(go.Scatter(y=m_fit.s_ev.events[option]['timeseries'].loc[:,0],
                                         x=m_fit.s_ev.timeseries.series_gauge.loc[event["start"]:event["end"], "Q"].index,
                                         mode="lines", name="fit"),
                              row=1, col=1)
                fig.show()

            with widget_out_evnt_txt:
                widget_out_evnt_txt.clear_output()

                print_event_info(m_fit, option)

    widget_season.observe(on_widget_season)
    widget_dd_hsep.observe(on_widget_dd_hsep)

    display(widgets.VBox([widgets.HBox([widget_season, widget_dd_hsep]),
                         widget_out_evnt, widget_out_evnt_txt]))


def show_return_period(dist_tb, display):
    """Show return period diagrams

    Two Drop down menues for selection of season and event.

    Parameters
    ----------
    dist_tb : hwlhw.Distribution
        Class with the distributions
    display : ipywidgets.display
        the display driver"""
    widget_out_evnt = widgets.Output()


    if len([x for x in dist_tb.parameter_season]) == 1:
        opts_seas = ['season_0']
    else:
        opts_seas = ['season_0', 'season_1']

    widget_season = widgets.Dropdown(
        options= opts_seas,
        description='Seasons',
        disabled=False)

    widget_param = widgets.Dropdown(
        options= ['t_a', 'm_an', 'm_ab', 'q_s', 't_p'],
        description='Parameter',
        disabled=False)

    widget_distr = widgets.Dropdown(
        options=  ['E1', 'AE', 'LN3', 'P3', 'LP3', 'WB3', 'ROV', 'Alle'],
        description='Verteilungen',
        disabled=False)

    widget_estr = widgets.Dropdown(
        options= ['MM', 'MLM', 'WGM', 'Alle'],
        description='Verteilungen',
        disabled=False)

    widget_show = widgets.Button(description="Anzeigen")

    def on_widget_show(c):
        with widget_out_evnt:
            widget_out_evnt.clear_output()
            season = widget_season.value
            param = widget_param.value
            distr = widget_distr.value
            if distr == 'Alle': distr = None
            estr = widget_estr.value
            if estr == 'Alle': estr = None
            dist_tb.return_period_charts(param, season=season, distribution=distr, estimator=estr)

    widget_show.on_click(on_widget_show)
    h1 = widgets.HBox([widget_season ])
    h2 = widgets.HBox([widget_param, widget_distr, widget_estr])
    h3 = widgets.HBox([widget_show])
    display(widgets.VBox([h1, h2, h3, widget_out_evnt]))


def show_fitted_fitness(dist_param, display):
    """Show return period diagrams

    Two Drop down menues for selection of season and event.

    Parameters
    ----------
    dist_param : hwlhw.Distribution
        Class with the distributions
    display : ipywidgets.display
        the display driver"""
    widget_out_evnt = widgets.Output()


    if len([x for x in dist_param.parameter_season]) == 1:
        opts_seas = ['season_0']
    else:
        opts_seas = ['season_0', 'season_1']

    widget_season = widgets.Dropdown(
        options= opts_seas,
        description='Seasons',
        disabled=False)

    widget_param = widgets.Dropdown(
        options= ['t_a', 'm_an', 'm_ab', 'q_s'],
        description='Parameter',
        disabled=False)

    widget_show = widgets.Button(description="Anzeigen")

    def on_widget_show(c):
        season = widget_season.value
        param = widget_param.value
        x = dist_param.modelvar_dist[season]['p'][param].df_prx
        with widget_out_evnt:
            widget_out_evnt.clear_output()
            display(x.iloc[:,-3:])

    widget_show.on_click(on_widget_show)
    display(widgets.VBox([widgets.HBox([widget_season, widget_param, widget_show]),
                         widget_out_evnt]))


class Show_fitted_event_bf:

    def __init__(self, m_fit, display):

        widget_out_evnt = widgets.Output()
        widget_out_evnt_txt = widgets.Output()
        widget_forward = widgets.Button(description=">")
        widget_backward = widgets.Button(description="<")
        widget_forward0 = widgets.Button(description=">>")
        widget_backward0 = widgets.Button(description="<<")

        self.eventlist = [x for x in m_fit.s_ev.events]
        self.lenx = len(self.eventlist)
        self.current_state = 0

        def on_widget_forward(args):
            self.current_state += 1
            self.current_state = 0 if self.current_state >= self.lenx else self.current_state

            on_widget()

        def on_widget_backward(args):
            self.current_state -= 1
            self.current_state = self.lenx if self.current_state == -1 else self.current_state
            on_widget()

        def on_widget_forward0(args):
            self.current_state += 10
            self.current_state = 0 if self.current_state >= self.lenx else self.current_state
            on_widget()

        def on_widget_backward0(args):
            self.current_state -= 10
            self.current_state = self.lenx if self.current_state < 0 else self.current_state
            on_widget()

        def on_widget():
            option = self.eventlist[self.current_state]
            event = m_fit.s_ev.events[option]
            with widget_out_evnt:
                widget_out_evnt.clear_output()
                fig = make_subplots(rows=1, cols=1)
                fig.add_trace(go.Scatter(y=m_fit.s_ev.timeseries.series_gauge.loc[event["start"]:event["end"], "Q"],
                                         x=m_fit.s_ev.timeseries.series_gauge.index,
                                         mode="lines", name="obs"),
                              row=1, col=1)
                fig.add_trace(go.Scatter(y=m_fit.s_ev.events[option]['timeseries'].loc[:,0],
                                          x=m_fit.s_ev.timeseries.series_gauge.index,
                                          mode="lines", name="fit"),
                              row=1, col=1)
                fig.update_layout(
                    title='{} / {} -- {}'.format(self.current_state+1, self.lenx, self.eventlist[self.current_state]),
                    xaxis_title="Datum/Zeit",
                    yaxis_title="Q",
                    font=dict(
                        family="Courier New, monospace",
                        size=12
                    )
                )
                fig.show()
            with widget_out_evnt_txt:
                widget_out_evnt_txt.clear_output()

                print_event_info(m_fit, option)

        widget_forward.on_click(on_widget_forward)
        widget_forward0.on_click(on_widget_forward0)
        widget_backward.on_click(on_widget_backward)
        widget_backward0.on_click(on_widget_backward0)

        display(widgets.VBox([widgets.HBox([widget_backward0, widget_backward, widget_forward, widget_forward0]),
                             widget_out_evnt, widget_out_evnt_txt]))


def show_fitted_event_bf(m_fit, display):
    Show_fitted_event_bf(m_fit, display)
