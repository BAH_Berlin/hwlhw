# -*- coding: utf-8 -*-
"""
Separate measured flow time series into individual flood events.
Single events can be given, or an text file with separated events imported.

Created on Sun May 30 18:32:32 2021
@author: ruben.mueller
"""

import os
import pickle
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


from scipy.signal import find_peaks, peak_prominences
from hwlhw.hwlhw_timeseries import Timeseries_gauge
from hwlhw.hwlhw_settings import DATEFORMAT_F
from hwlhw.hwlhw_model_tb import TB_MODELMAP

def calc_period(ss, se) -> list:
    '''Return two list with months (seasons) such that
    ss is the beginning of the first season and se the end of the first season.
    the second season includes all other months. ss and se are int {1,2,...,12}

    Parameters
    ----------
    ss : int
        the start of season1
    se : int
        the end of season2
    '''
    ss = int(ss)
    se = int(se)
    if ss > se:
        a = list(range(ss, 13)) + list(range(1, se+1))
        b = list(range(se+1, ss))
    else:
        a = list(range(ss, se+1))
        b = list(range(se+1, 13)) + list(range(1, ss))
    return a, b


class Separate_events:
    """Class for the seperation of flood events."""

    def __init__(self, timeseries, override=False):
        """Seperate flood events.

        Parameters
        ----------
        timeseries : Timeseries_gauge
            The timeseries with observed flows."""

        self.events = {}
        if not override:
            self.qo = timeseries.qo
            if not isinstance(timeseries, Timeseries_gauge):
                raise TypeError("events has to be of type %s", Separate_events)
            self.timeseries = timeseries
            self.period = self.timeseries.period
            self.freq = self.timeseries.freq
        else:
            self.qo = None
            self.timeseries = None
            self.period = None
            self.freq = None
        
        self.fB = 1
        self._eventcounter = 0
        self.seasons = [list(range(1, 13))]
        self.seasonal_stats = {}
        self.file = None
        self.mom_treshold = 3
        self.distance = 4*24
        self.time_asc_fact = 2


    def set_seasons(self, start_month=11, end_month=10):
        """Divide the year into two seasons.

        Parameters
        ----------
        start_month : int, optional
            Fist month of the first season. The default is 11.
        end_month : int, optional
            The last month of the first season. The default is 4."""

        self.seasons = calc_period(start_month, end_month)
        self.seasons = self.seasons if self.seasons[1] else [self.seasons[0]]
        [self.seasonal_stats.update({i: {'t_a': datetime.timedelta(minutes=0), 'q_s':0}}) for i, _ in enumerate(self.seasons)]



    def read_events_file(self, workspace, filename,
                         seperator=';', decimal=',', peak='end'):
        """Read the events declaration file.

        Parameters
        ----------
        workspace : str
            The workspace folder.
        filename : str
            The file name.
        seperator : str, optional
            The colmn seperator. The default is ';'.
        decimal : str, optional
            the decimal sign. The default is ','.
        peak : str
            | start : peak is at the first date of pleateau
            | middle : peak is at the middle date of pleateau
            | end : peak is at the end date of pleateau"""

        self.file = os.path.join(workspace, filename)

        if not os.path.isfile(self.file):
            raise FileNotFoundError("Event-Datei %s nicht gefunden." % self.file)

        def lmbda(x):
            return datetime.datetime.strptime(x, DATEFORMAT_F["minute"])

        with open(self.file, 'r') as fid:
            events = pd.read_csv(fid, sep=seperator, skiprows=0,
                    header=1, decimal=decimal,
                    date_parser=lmbda)

        for event in events.index:
            self.add_event(events.loc[event, "Start"], events.loc[event, "End"],
                           alt_Q=events.loc[event, "Alternative_Peak"],
                           peak=peak)


    def _stat_qs(self, val, mean):
        if not self._eventcounter:
            mean = val
        else:
             mean = (mean*self._eventcounter + val) / (self._eventcounter+1)
        return mean


    def add_event(self, start, end, alt_Q=None, dformat="%d.%m.%Y %H:%M", peak="end"):
        """Add an event to the events list.

        Parameters
        ----------
        start : str or datetime.datetime
            the begining of the event
        end : str or datetime.datetime
            the ending og the event
        alt_Q : str or datetime.datetime
            set the time of the peak
        dformat : str
            the format in which the dates are given
        peak : str
            | start : peak is at the first date of pleateau
            | middle : peak is at the middle date of pleateau
            | end : peak is at the end date of pleateau"""

        if dformat == "Tag":
            dformat = "%d.%m.%Y"
        elif dformat == "Stunde":
            dformat = "%d.%m.%Y %H:%M"

        if isinstance(start, datetime.datetime):
            s1 = start
        elif isinstance(start, str):
            s1 = datetime.datetime.strptime(start, dformat)
        else:
            TypeError("argument start must not be of type %s" % start)

        if s1 < self.timeseries.series_gauge.index[0]:
            raise ValueError("start is before the beginning of the time series, %s < %s" % s1, self.timeseries.index[0])

        if isinstance(end, datetime.datetime):
            e1 = end
        elif isinstance(end, str):
            e1 = datetime.datetime.strptime(end, dformat)
        elif isinstance(end, datetime.timedelta):
            e1 = s1 + end
        else:
            TypeError("argument start must not be of type %s" % start)

        try:
            name = ""
            if isinstance(alt_Q, str):
                ts = datetime.datetime.strptime(alt_Q, dformat)
                qs = self.timeseries.series_gauge.loc[ts, "Q"]
            else:
                ts = self.timeseries.series_gauge.loc[s1:e1, "Q"].idxmax()
                qs = self.timeseries.series_gauge.loc[ts, "Q"]
                if peak == 'middle':
                    sect = self.timeseries.series_gauge.loc[s1:e1, "Q"]
                    idx = sect.index.get_loc(sect[sect == qs].index[0] + (sect[sect == qs].index[-1] - sect[sect == qs].index[0])/2,
                                             method='nearest')
                    ts = sect.index[idx]
                elif peak == 'end':
                    sect = self.timeseries.series_gauge.loc[s1:e1, "Q"]
                    idx = sect.index.get_loc(sect[sect == qs].index[-1],
                                             method='nearest')
                    ts = sect.index[idx]
                else:
                    ts = self.timeseries.series_gauge.loc[s1:e1, "Q"].idxmax()
                qs = self.timeseries.series_gauge.loc[ts, "Q"]


            if self.seasons is None:
                season = 0
            else:
                season = [i for i, x in enumerate(self.seasons) if s1.month in x][0]


            name = 'TS_{}_Q_{}'.format(datetime.datetime.strftime(ts, "%Y-%m-%dT-%H:%M"), qs)

            self.events.update({name: {"start": s1, "end": e1,
                                       "Qs": qs,
                                       "ts": ts,
                                       "ta": ts - s1,
                                       "Qbase": self.timeseries.series_gauge.loc[s1, "Q"] * self.fB,
                                       "parameter": None,
                                       "fitness": None,
                                       "fitnesstype": None,
                                       "model": None,
                                       "vol_obs": None,
                                       "vol_sim":None,
                                       "qo": self.timeseries.qo,
                                       "period": self.timeseries.period,
                                       "season": season,
                                       "freq": self.timeseries.freq}})
            self.seasonal_stats[season]['t_a'] = max(self.seasonal_stats[season]['t_a'],
                                                     self.events[name]['ta'])
            self.seasonal_stats[season]['q_s'] = self._stat_qs(self.events[name]['Qs'], self.seasonal_stats[season]['q_s'] )
            self._eventcounter += 1
        except Exception as e:
            print(e, "for event", name, s1, e1)


    def events_peak_01(self, mq=None, mom_treshold=3, distance=4*24, time_asc_fact=2):
        """Separate events

        Parameters
        ----------
        mq : float
            mean flow override
        mom_treshold : float, optional
            treshold is multiple over the mean flow for peak search. The default is 3.
        distance : int, optional
            minimum timesteps between peaks. The default is 4*24.
        time_asc_fact : int, optional
            period of time after the peak computed as ascending time times time_asc_fact. The default is 2."""


        x = self.timeseries.series_gauge.values[:, 0]
        y = self.timeseries.series_gauge.index
        if mq is None:
            mq = np.mean(x)
        peaks, p2 = find_peaks(x, distance=distance, height=mq*mom_treshold)

        #idx_s1 = 0
        for i, p in enumerate(peaks[:-1]):
            idx_s1 = np.argmin(x[peaks[i]-distance:peaks[i]]) + peaks[i]-distance
            time_asc = peaks[i] - idx_s1

            nb = np.min((peaks[i+1]-peaks[i], time_asc*time_asc_fact))
            mx = np.argmin(x[peaks[i]:peaks[i]+nb]) + peaks[i]

            s1 = y[idx_s1]
            e1 = y[mx]
            ts =  self.timeseries.series_gauge.loc[s1:e1, "Q"].idxmax()

            qs =  self.timeseries.series_gauge.loc[ts, "Q"]
            if self.seasons is None:
                season = 0
            else:
                season = [i for i, x in enumerate(self.seasons) if s1.month in x][0]

            name = 'TS_{}_Q_{}'.format(datetime.datetime.strftime(ts, "%Y-%m-%dT-%H:%M"), qs)
            self.events.update({name: {"start": s1, "end": e1,
                                    "Qs": qs,
                                    "ts": ts,
                                    "ta": ts - s1,
                                    "Qbase":  self.timeseries.series_gauge.loc[s1, "Q"],
                                    "parameter": None,
                                    "fitness": None,
                                    "fitnesstype": None,
                                    "model": None,
                                    "vol_obs": None,
                                    "vol_sim":None,
                                    "qo":  self.timeseries.qo,
                                    "period":  self.timeseries.period,
                                    "season": season,
                                    "freq":  self.timeseries.freq}})


    def export_separated_events(self, workspace):
        """Export the separated events into a CSV file in the workspace."""
        with open(os.path.join(workspace, 'separated_events_{}_{}_{}.csv'.format(self.mom_treshold,
                                                                                 self.distance,
                                                                                 self.time_asc_fact)), 'w') as fid:
            fid.write('Start;End;Alternative_Peak\n')
            for k, event in self.events.items():
                fid.write("{};{};{}\n".format(datetime.datetime.strftime(event["start"], "%d.%m.%Y %H:%M"),
                                              datetime.datetime.strftime(event["end"], "%d.%m.%Y %H:%M"),
                                                                         ''))


    def reset_events(self):
        """"Clear all even from the events list."""
        self.events = {}
        self._eventcounter = 0


    def show_events_timeseries(self, figure=None):
        """Plot all events highlighted as grey areas in the flow time series."""
        if self.timeseries is None:
            print("Please read the time series data first. %s is empty." % self.timeseries)
        fig, ax = plt.subplots(1, 1)
        self.timeseries.series_gauge.loc[:, "Q"].plot(ax=ax, label="Q")
        for event in self.events.items():
            ax.axvspan(event[1]["start"], event[1]["end"], facecolor='gray', alpha=0.5)
        plt.grid()
        plt.legend()
        plt.xlabel("Datum")
        plt.ylabel(r"$\mathrm{Q}\quad (\mathrm{m^{3}s^{-1}})$")
        plt.title(self.timeseries.gauge)
        fig.show()


    def maximum_flow(self, qo):
        """Set the maximum flow, default 2*max(timeseries)"""
        self.qo = qo


    def save_state(self, workspace, file='Separated_events.pkl'):
        """save the state of the class"""
        tmp = {'events': self.events,
            'qo': self.qo,
            'fB': self.fB,
            '_eventcounter': self._eventcounter,
            'seasons': self.seasons,
            'seasonal_stats': self.seasonal_stats,
            'period': self.period,
            'freq': self.freq,
            'file': self.file,
            'mom_treshold': self.mom_treshold,
            'distance': self.distance}

        with open(os.path.join(workspace, file), 'wb') as fid:
            pickle.dump(tmp, fid, pickle.HIGHEST_PROTOCOL)


    @staticmethod
    def load_state(workspace, timeseries, file='Separated_events.pkl'):
        with open(os.path.join(workspace, file), 'rb') as fid:
            tmp = pickle.load(fid)

            ns = Separate_events(timeseries)
            for k in tmp:
                setattr(ns, k, tmp[k])
        return ns
