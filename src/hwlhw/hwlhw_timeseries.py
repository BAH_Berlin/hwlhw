# -*- coding: utf-8 -*-
"""
Import measured flow time series from basic CSV-files or WISKI-CSV files.

Created on Sun May 30 16:36:15 2021
@author: ruben.mueller
"""

import os
import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from hwlhw.hwlhw_settings import DATEFORMAT_F

class Timeseries_gauge:
    """Class for importing the flow time series."""

    def __init__(self, gauge):
        """Import the measured flow time series.

        Parameters
        ----------
        gauge : str
            name of the gauge.
        """
        self.length = 0
        self.mq = 0
        self.series_gauge = None
        self.df_hq_year = None
        self.qsmax_idx = []
        self.qsmax = []
        self.gauge = gauge
        self.df_hq_hydyear = None
        self.period = None
        self.freq = None
        self.qo = None
        self.file = None
        self.hhq_multiplier = 10

    def read_csv(self, folder, filename, timestep="hour",
                 seperator="\t", decimal=".", nancode=-999) -> None:
        """Read the measured flow time series from a CSV file.

        Parameters
        ----------
        folder : str
            the folder with the file
        filename : str
            the file name
        timestep : str
            the time step from "day", "hour", "minute"
        seperator : str
            the seperator used in the file
        decimal : str
            the used decimal
        nancode : str of number
            the missing value marker that is replaced by NaN values"""
        if not timestep in ("day", "hour", "minute"):
            raise ValueError("Timestep must be 'minute' or 'day'")

        if not os.path.isdir(folder):
            raise OSError("Folder %s not found" % folder)
        if not os.path.isfile(filename):
            raise OSError("File %s not found" % filename)

        self.file = os.path.join(folder, filename)
        def lmbda(x):
            return datetime.datetime.strptime(x, DATEFORMAT_F[timestep])

        with open(self.file) as fid:
            self.series_gauge = pd.read_csv(fid, sep=seperator,
                                            header="infer", decimal=decimal,
                                            date_parser=lmbda, index_col="Termin")

            self.series_gauge[self.series_gauge == nancode] = np.nan

        if not isinstance(self.series_gauge, pd.core.frame.DataFrame):
            raise ValueError("Unspecified error reading %s" % filename)

        self.period = (self.series_gauge.index[1] - self.series_gauge.index[0]).value / 1000000000
        self.qo = self.hhq_multiplier * self.series_gauge.loc[:,"Q"].max()
        self.freq = pd.infer_freq(self.series_gauge.index)


    def read_wiski(self, folder, filename, timestep="minute",
                   seperator=";", decimal=",", nancode=-999) -> None:
        """Read the measured flow time series from a WISKI file.

        Parameters
        ----------
        folder : str
            the folder with the file
        filename : str
            the file name
        timestep : str
            the time step from "day", "hour", "minute"
        seperator : str
            the seperator used in the file
        decimal : str
            the used decimal
        nancode : str of number
            the missing value marker that is replaced by NaN values"""
        if not timestep in ("day", "minute"):
            raise ValueError("Timestep must be 'minute' or 'day'")

        self.file = os.path.join(folder, filename)
        if not os.path.isdir(folder):
            raise OSError("Folder %s not found" % folder)
        if not os.path.isfile(self.file):
            raise OSError("File %s not found" % self.file)

        self.file = os.path.join(folder, filename)
        def lmbda(x):
            return datetime.datetime.strptime(x, DATEFORMAT_F[timestep])

        with open(self.file, 'r') as fid:
            self.series_gauge = pd.read_csv(fid, sep=seperator,
                                            header='infer', decimal=decimal,
                                            date_parser=lmbda, index_col=0,
                                            usecols=['Datum/Zeit', 'Wert [Kubikmeter pro Sekunde]'])
            self.series_gauge = self.series_gauge.rename(columns={'Wert [Kubikmeter pro Sekunde]': "Q"})

            self.series_gauge[self.series_gauge == nancode] = np.nan

        if not isinstance(self.series_gauge, pd.core.frame.DataFrame):
            raise ValueError("Unspecified error reading %s" % filename)

        self.period = (self.series_gauge.index[1] -self.series_gauge.index[0]).value/1000000000
        self.qo = self.hhq_multiplier * self.series_gauge.loc[:,"Q"].max()
        self.freq = pd.infer_freq(self.series_gauge.index)


    def get_hqmax_year(self, print_result=False) -> None:
        """Get the highest flow for each calendar year.

        Parameters
        ----------
        print_result : bool
            print the results if True"""
        self.df_hq_year = self.series_gauge.loc[
            self.series_gauge.groupby(self.series_gauge.index.year).idxmax().loc[:, "Q"]]
        if print_result:
            print(self.df_hq_year)


    def get_hqmax_hydyear(self, hyear_start=11, hyear_end=10) -> None:
        """Get the highest flow for each hydrological year.
        Provide both parameter for user selected years.

        Parameters
        ----------
        hyear_start : int
            first month of the hydological year
        hyear_end : int
            last month of the hydological year."""
        self.qsmax_idx = []
        self.qsmax = []
        if self.series_gauge is not None:
            years = np.unique(self.series_gauge.index.year)
            for year in years:
                # cyf = self.series_gauge[(self.series_gauge.index.year == year & self.series_gauge.index.month >= 11)]

                ts1 = (self.series_gauge[
                    (self.series_gauge.index.year == year) & (self.series_gauge.index.month >= hyear_start)]).append(
                    self.series_gauge[
                        (self.series_gauge.index.year == year + 1) & (self.series_gauge.index.month <= hyear_end)])
                try:
                    self.qsmax.append(ts1.loc[ts1.idxmax().loc["Q"]])
                    self.qsmax_idx.append(ts1.idxmax().loc["Q"])
                except:
                    print("Year %s not fully available" % year)

        self.df_hq_hydyear = pd.DataFrame({"Termin": self.qsmax_idx, "Q": [x.loc["Q"] for x in self.qsmax]})
        self.df_hq_hydyear.set_index('Termin', inplace=True)


    def plot_timeseries(self) -> None:
        """Plot the flow time series."""
        if not self.series_gauge is None:
            fig, ax = plt.subplots(1, 1)
            self.series_gauge.loc[:, "Q"].plot(ax=ax)
            ax.xaxis.grid()
            ax.yaxis.grid()
            plt.xlabel("Datum")
            plt.ylabel(r"$\mathrm{Q}\quad (\mathrm{m^{3}s^{-1}})$")
            plt.title(self.gauge)


    def plt_hqmax_year(self) -> None:
        """Plot the flow time series with the HQ events for calendar years and hydrolgocial years."""
        if not self.series_gauge is None:
            fig, ax = plt.subplots(1, 1)
            self.series_gauge.loc[:, "Q"].plot(ax=ax, label="Q")
            if not self.df_hq_year is None:
                self.df_hq_year.loc[:, "Q"].plot(ax=ax, marker="v", linestyle='None',
                                                 markeredgecolor='k', markerfacecolor='None',
                                                 label=r"$\mathrm{Q_{s,\;Jahr}}$")
            if self.qsmax_idx:
                self.series_gauge.loc[self.qsmax_idx, "Q"].plot(ax=ax, marker="^", linestyle='None',
                                                                markeredgecolor='k', markerfacecolor='None',
                                                                label=r"$\mathrm{Q_{s,\;hyd.\;Jahr}}$")
            ax.xaxis.grid()
            ax.yaxis.grid()
            plt.legend()
            plt.xlabel('Datum')
            plt.ylabel(r'$\mathrm{Q}\quad (\mathrm{m^{3}s^{-1}})$')
            plt.title(self.gauge)
