# -*- coding: utf-8 -*-
"""
Utiility functions for the hwlhw package.
Save and load work with binary files and plot diagrams.

Created on Sun May 30 18:46:12 2021
@author: ruben.mueller
"""
import sys
import pickle

import numpy as np
import traceback

from plotly.subplots import make_subplots

import plotly.graph_objects as go
from hwlhw.hwlhw_model_tb import TB_MODELMAP
from hwlhw.hwlhw_settings import (DISTF_MAP, DIST_MAP_PARN, MAPPER)


def update_progress(progress, param=""):
    '''prints a nice status bar and updates it, if called again.

    Paramters
    ---------
    progress: float
        the fraction of work done --> iter/float(length(Iterable))'''

    barLength = 10 # Modify this to change the length of the progress bar
    status = ""
    
    if isinstance(progress, list):
        progress, param = progress
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2} -- {3}".format("#"*block \
           + "-"*(barLength-block), round(progress*100, 4), status,
           param)
    sys.stdout.write(text)
    sys.stdout.flush()



def save_obj(name, obj, ending='pkl'):
    '''Save a struct or list into a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file
        without file ending, functions add .pkl
    obj : struct, list,...
        object to save in prickle file'''
    len_end = len(ending)
    if len(name) > len_end+2:
        if name[-len_end:] == ending:
            name = name[:-len_end-1]
    try:
        with open(name + '.' + ending, 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL) #L
            #print('saved')
    except Exception as e:
        raise IOError('could not write prickle file {}: {}'.format(name, e))




def load_obj(name, ending='pkl'):
    '''Load a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file'''
    len_end = len(ending)
    if len(name) > len_end+2:
        if name[-len_end:] == ending:
            name = name[:-len_end-1]
    try:
        with open(name + '.' + ending, 'rb') as f:
            if sys.version_info[0] < 3:
                return pickle.load(f)
            else:
                return pickle.load(f, encoding='bytes')
    except FileExistsError:
        raise FileExistsError('could not read prickle file {}'.format(name))




def plot_scatter(par_x, par_y, hqsim, m_fit, mc_mean='mc', figure=None):
    """Scatterplots to compare two variables derived from observations and fitted models.

    Parameters
    ----------
    par_x : str
        Variable for the x-axis.
    par_y : str
        Variable for the y-axis.
    hqsim : HQ_simulation
        Class for HQ-event simulation.
    m_fit : Model_fitter
        Class for event simulation.
    mc_mean : str, optional
        | Select mc for Monte-Carlo-method
        | and mean for the mean event method. The default is 'mc'."""
    legel_var = [x for x in MAPPER]
    for p in (par_x, par_y):
        if not p in (legel_var):
            raise ValueError('par_x and par_y must be from', legel_var)


    fig = make_subplots(rows=1, cols=len(m_fit.s_ev.seasons))
    if mc_mean == 'mean':
        if not len(hqsim.hq_events_m):
            raise ValueError('No mean event simulation done. Set mc_mean to "mc" to plot Monte-Carlo.')
    else:
        if not len(hqsim.hq_events):
            raise ValueError('No Monte-Carlo simulation done. Set mc_mean to "mean" to plot mean event.')


    def from_manual(hqsim, seas):
        events = hqsim.hq_events_m['season_{}'.format(seas)]['events']
        model = hqsim.hq_events_m['season_{}'.format(seas)]['modelname']
        pars1 = TB_MODELMAP[model]['parameter']
        pars2 = TB_MODELMAP[model]['parameter_fixed']
        #
        # SIM
        #
        for eventsim in events.items():
            try:
                x = MAPPER[par_x]['func_sim'](eventsim[1], MAPPER[par_x]['sim'], pars1)
            except:
                x = MAPPER[par_x]['func_simB'](eventsim[1], MAPPER[par_x]['sim'], pars2)
            try:
                y = MAPPER[par_y]['func_sim'](eventsim[1], MAPPER[par_y]['sim'], pars1)
            except:
                y = MAPPER[par_y]['func_simB'](eventsim[1], MAPPER[par_y]['sim'], pars2)
            fig.add_trace(go.Scatter(x=[x],
                                     y=[y],
                                     marker=dict(
                                        color='grey',
                                        size=8),
                                     #name=eventsim[1],
                                     name='Simulation'
                                     ),
                          row=1, col=seas+1)
        return pars1, pars2, p


    def from_mc(hqsim, seas):
        events = hqsim.hq_events['season_{}'.format(seas)]['events']
        model = hqsim.hq_events['season_{}'.format(seas)]['modelname']
        pars1 = TB_MODELMAP[model]['parameter']
        pars2 = TB_MODELMAP[model]['parameter_fixed']
        #
        # SIM
        #
        for eventsim in events:
            try:
                x = MAPPER[par_x]['func_sim'](eventsim, MAPPER[par_x]['sim'], pars1)
            except:
                x = MAPPER[par_x]['func_simB'](eventsim, MAPPER[par_x]['sim'], pars2)
            try:
                y = MAPPER[par_y]['func_sim'](eventsim, MAPPER[par_y]['sim'], pars1)
            except:
                y = MAPPER[par_y]['func_simB'](eventsim, MAPPER[par_y]['sim'], pars2)
            fig.add_trace(go.Scatter(x=[x],
                                     y=[y],
                                     marker=dict(
                                        color='grey',
                                        size=8),
                                     name='Simulation'
                                     ),
                          row=1, col=seas+1)
        return pars1, pars2, p

    # -----------------------------------------------------------------------
    for seas, x in enumerate(m_fit.s_ev.seasons):
        #seas = seasx + 1
        #
        # SIM
        #
        if mc_mean == 'mean':
            pars1, pars2, p = from_manual(hqsim, seas)
        else:
            pars1, pars2, p = from_mc(hqsim, seas)

        #
        # OBS
        #
        for eventobs in m_fit.s_ev.events.items():

            if eventobs[1]['season'] == seas:

                if par_x == 'q_s':
                    x = eventobs[1]['Qs']
                elif par_x == 't_a':
                    x = eventobs[1]['ta'].total_seconds() / 3600
                else:
                    try:
                        x = MAPPER[par_x]['func_obs'](eventobs[1], MAPPER[par_x]['obs'], pars1)
                    except:
                        x = MAPPER[par_x]['func_obsB'](eventobs[1], MAPPER[par_x]['obs'], pars2)

                if par_y == 'q_s':
                    y = eventobs[1]['Qs']
                elif par_y == 't_a':
                    y = eventobs[1]['ta'].total_seconds() / 3600
                else:
                    try:
                        y = MAPPER[par_y]['func_obs'](eventobs[1], MAPPER[par_y]['obs'], pars1)
                    except:
                        y = MAPPER[par_y]['func_obsB'](eventobs[1], MAPPER[par_y]['obs'], pars2)

                fig.add_trace(go.Scatter(x=[x],
                                         y=[y],
                                         marker=dict(
                                            color='steelblue',
                                            size=10),
                                         text=eventobs[0],
                                         name='Beobachtung'
                                         ),
                              row=1, col=seas+1)
        if len(m_fit.s_ev.seasons) == 1:
            fig.update_layout(
                title="Saison 0",
                xaxis_title=MAPPER[par_x]['label'],
                yaxis_title=MAPPER[par_y]['label'],
                font=dict(
                    family="Courier New, monospace",
                    size=12
                )
            )
        else:
            fig.update_layout(
                title="Saison 1",
                xaxis_title=MAPPER[par_x]['label'],
                yaxis_title=MAPPER[par_y]['label'],
                xaxis2_title=MAPPER[par_x]['label'],
                yaxis2_title=MAPPER[par_y]['label'],
                font=dict(
                    family="Courier New, monospace",
                    size=12
                )
            )

    fig['data'][0]['showlegend'] = True
    fig['data'][0]['name'] = 'Simulation'

    fig['data'][-1]['showlegend'] = True
    fig['data'][-1]['name'] = 'Beobachtung'

    fig.update_layout(showlegend=False)
    if figure is None:
        fig.show()
    else:
        return fig



def plot_volume_precipitation(m_fit, hqsim, area_km2, figure=None):
    """Plot the ascending time against the effective precpitation.

    Parameters
    ----------
    m_fit : Model_fitter
        Class for event simulation.
    hqsim : HQ_simulation
        Class for HQ-event simulation.
    area_km2 : float
        The catchment area in square kilometers."""
    fig = make_subplots(rows=1, cols=len(m_fit.s_ev.seasons))
    for seas, x in enumerate(m_fit.s_ev.seasons):

        parameternames1 = TB_MODELMAP[m_fit.models[seas].name]['parameter']
        parameternames2 = TB_MODELMAP[m_fit.models[seas].name]['parameter_fixed']


        list_ta = np.zeros(len(hqsim.hq_events['season_{}'.format(seas)]['events']))
        list_vol = np.zeros(len(hqsim.hq_events['season_{}'.format(seas)]['events']))

        events = hqsim.hq_events['season_{}'.format(seas)]['events']
        for i, eventsim in enumerate(events):
            if 't_a' in parameternames1:
                idx = parameternames1.index('t_a')
                list_ta[i] = eventsim['parameter'][idx]/4
            else:
                idx = parameternames2.index('t_a')
                list_ta[i] = eventsim['parameter_fixed'][idx]/4

            list_vol[i] = eventsim['vol_sim'] *60*15 / area_km2 / 1000

        ### select the max val for all t_a
        ta = []
        vl = []
        indiv_t_a = np.unique(list_ta)
        for ita in indiv_t_a:
            where1 = np.argwhere(list_ta == ita)
            where2 = np.argmax(list_ta[where1])
            ta.append(list_ta[where1][where2])
            vl.append(list_vol[where1][where2])

        fig.add_trace(go.Scatter(x=np.concatenate(ta),
                                 y=np.concatenate(vl),
                                 marker=dict(
                                    color='steelblue',
                                    size=8),
                                 mode="markers"
                                 ),
                      row=1, col=seas+1)
        if len(m_fit.s_ev.seasons) == 1:
            fig.update_layout(
                title='Saison {}'.format(seas),
                xaxis_title='Regendauer (h) [~Anlaufzeit]',
                yaxis_title='Niederschlag (mm)',
                font=dict(
                    family="Courier New, monospace",
                    size=12
                )
            )
        else:
            fig.update_layout(
                title='Saison {}'.format(seas),
                xaxis_title='Regendauer (h) [~Anlaufzeit]',
                yaxis_title='Niederschlag (mm)',
                xaxis2_title='Regendauer (h) [~Anlaufzeit]',
                yaxis2_title='Niederschlag (mm)',
                font=dict(
                    family="Courier New, monospace",
                    size=12
                )
            )
        if figure is None:
            fig.show()
        else:
            return fig




def cdf_array(p, distr, est, mx, resolution=200):
    vvec = np.zeros((3, resolution))
    vvec[1, :] = np.linspace(1E-4, mx, resolution)
    try:
        for i, qs in enumerate(vvec[1, :]):
            vvec[0, i] = DISTF_MAP[distr](qs, *p.loc[(distr, est),
                                                     DIST_MAP_PARN[distr]].values.tolist())
        vvec[2, :] = 1/(1-vvec[0, :])
    except Exception as e:
        print(e)
        traceback.print_exc()
    return vvec



W11 = [0.1, 0.5, 0.8, 0.9, 0.95, 0.96, 0.98, 0.99, 0.995,  0.998, 0.999]
W10 = [0.5, 0.8, 0.9, 0.95, 0.96, 0.98, 0.99, 0.995,  0.998, 0.999]



def cdf_array2(p, distr, est):
    h_list = p.loc[(distr, est), :].values.tolist()
    lenx = len(h_list)
    p1 = W11 if lenx == 11 else W10

    vvec = np.zeros((3, lenx+1))
    vvec[1, 1:] = h_list
    vvec[0, 1:] = p1
    vvec[2, :] = 1/(1-vvec[0, :])
    return vvec
