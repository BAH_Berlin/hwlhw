# -*- coding: utf-8 -*-
"""
Fitness criteria for the form parameter optimization of the flood event models.

Please notice, that you need to set the correct factor in the models when returning the fitness criteria.
The optimizer minimizes the fitness, therefore, for KlingGuptaEfficiency, R2, relNSE you need to multiply with an facotr of -1.
Change the code accordingly. Future versions will correct for this automatically.

Created on Sun May 30 16:50:13 2021
@author: ruben.mueller
"""

import numpy as np
import numpy.ma as ma

def rmse(sim, obs) -> np.array:
    '''Calculate the root mean squared error.'''
    return np.sqrt(np.nansum((obs-sim)**2) / len_no_nan(obs))

def len_no_nan(array) -> np.array:
    """Get the length of an array excluding nan values."""
    return len(array) - np.count_nonzero(np.isnan(array))

def coeffVar(data):
    """Calulate the coefficient of variation."""
    return np.nanvar(data)/(np.nanmean(data)+np.finfo(float).eps)

def KlingGuptaEfficiency(sim, obs, rowvar=True):
    """Calulate the Kling-Gupta efficiency."""
    a=ma.masked_invalid(obs)
    b=ma.masked_invalid(sim)
    #msk = (~a.mask & ~b.mask)
    corrTerm = np.power((ma.corrcoef(a, b, rowvar=False)[0, 1] - 1), 2)
    biasTerm = np.power(np.nanmean(sim) / np.nanmean(obs) - 1, 2)
    variTerm = np.power(coeffVar(sim)/(coeffVar(obs)+np.finfo(float).eps) - 1, 2)
    return 1 - np.power(corrTerm + biasTerm + variTerm, 0.5)

def relNSE(sim, obs):
    '''Calculate the relative Nash-Sutcliff-Efficiency.'''
    mean_obs = np.nanmean(obs)
    return 1- np.nansum(((obs-sim)/(obs+0.00000001))**2) \
              /np.nansum(((obs-mean_obs)/mean_obs)**2)

def dValue(sim, obs):
    '''Calculate the d measure.'''
    mean_obs = np.nanmean(obs)
    return np.nansum((obs-sim)**2) \
           /np.nansum((np.abs(sim-mean_obs) + np.abs(obs-mean_obs))**2)

def R2(sim, obs):
    '''Calculate the R-squared measure.'''
    mean_sim = np.nanmean(sim)
    mean_obs = np.nanmean(obs)
    return (np.nansum((obs-mean_obs)*(sim-mean_sim))
            /((np.nansum((obs-mean_obs)**2))**0.5 \
            *(np.nansum((sim-mean_sim)**2))**0.5))**2