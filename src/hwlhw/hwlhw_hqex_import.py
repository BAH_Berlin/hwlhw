# -*- coding: utf-8 -*-
"""
Import EQEX-Files with parameters of fitted distributions and HQ(T).

Created on Sun May 30 16:50:13 2021
@author: ruben.mueller
"""

import os
import pandas as pd
import numpy as np
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------


class Hqex_dist_para:
    """Class for importing HQ-EX fitted parameter and HQ(T) results by reading the files."""

    def __init__(self):
        self.__filename = None
        self.__folder = None
        self.df_stx = None
        self.df_prx = None

    @property
    def filename(self):
        return self.__filename

    @filename.setter
    def filename(self, filename):
        if filename[-4] == ".":
            filename = filename[:-4]
        self.__filename = filename

    @property
    def folder(self):
        return self.__folder

    @folder.setter
    def folder(self, folder):
        if not os.path.isdir(folder):
            raise OSError("Folder %s not found" % folder)
        self.__folder = folder


    def read_stx_file(self) -> None:
        """Read the results files from HQ-EX."""

        # ---------------------
        # Read the STX file
        # ---------------------
        f1 = '--'
        if self.__folder is None:
            raise ValueError("Please set the folder first")
        if self.__filename is None:
            raise ValueError("Please set the filename first")
        f1 = os.path.join(self.__folder, self.__filename + '.stx')
        if not os.path.isfile(f1):
            raise FileNotFoundError("File %s not found" % f1)

        with open(f1, 'r') as fid:
            lines_list = fid.readlines()

        for idx, line in enumerate(lines_list):
            if line[:20] == "Wahrscheinlichkeiten":
                break

        header = lines_list[idx-1][29:].split()
        index = pd.MultiIndex.from_arrays(
                        [[x.split()[0] for x in lines_list[idx+1:-2]],
                         [x.split()[1] for x in lines_list[idx+1:-2]]],
                        names=['dist', 'est'])

        data = np.zeros((len(lines_list)-idx-3, len(header)))

        for i, line in enumerate(lines_list[idx+1:-2]):
            dline = [float(x) for x in line.split()[2:]]
            data[i, :] = dline

        tmp = pd.DataFrame(data, index=index)

        rename_dict = {}
        [rename_dict.update({tmp.columns[i]: "HQ("+h+')'}) for i, h  in enumerate(header)]

        self.df_stx  = tmp.rename(columns=rename_dict)


    def read_prx_file(self) -> None:
        """"""
        if self.__folder is None:
            raise ValueError("Please set the folder first")
        if self.__filename is None:
            raise ValueError("Please set the filename first")
        # ---------------------
        # Read the PRX file
        # ---------------------
        f2 = os.path.join(self.__folder, self.__filename + '.prx')
        if not os.path.isfile(f2):
            raise FileNotFoundError("File %s not found" % f2)

        with open(f2, 'r') as fid:
            lines_list = fid.readlines()

        for idx, line in enumerate(lines_list):
            if line[:12] == "Anzahl Werte":
                break

        header = lines_list[idx+1].split()
        index = pd.MultiIndex.from_arrays(
                        [[x.split()[0] for x in lines_list[idx+2:]],
                         [x.split()[1] for x in lines_list[idx+2:]]],
                        names=['dist', 'est'])

        data = np.zeros((len(lines_list)-idx-2, len(header)))

        for i, line in enumerate(lines_list[idx+2:]):
            dline = [float(x) for x in line.split()[2:]]
            if len(dline) == 5:
                dline.insert(2, np.NaN)
                dline.insert(2, np.NaN)
            if len(dline) == 6:
                dline.insert(3, np.NaN)
            data[i, :] = dline

        tmp = pd.DataFrame(data, index=index)

        rename_dict = {}
        [rename_dict.update({tmp.columns[i]: h}) for i, h  in enumerate(header)]

        self.df_prx = tmp.rename(columns=rename_dict)


    def get_parameter_pd(self) -> pd.DataFrame:
        """Get a pandas dataframe with parameters and criteria."""
        return self.df_prx


    def get_hqt_pd(self) -> pd.DataFrame:
        """Get a pandas dataframe with flows for return periods."""
        return self.df_stx


    def get_distributions(self, dist='P3') -> list:
        """Get a list with all imported distributions."""

        if dist == "all":
            lst = [a[0] + " " + a[1] for a in self.df_prx.index.to_list() if a[0] == dist]
        else:
            lst = [a[0] + " " + a[1] for a in self.df_prx.index.to_list()]

        return lst

if __name__ == "__main__":
    hqex_results = Hqex_dist_para()
    hqex_results.folder = "D:\\BAH_sync\\nietro_\\LHW\\N2\\Input"
    hqex_results.filename = "elend_1951-2019"
    hqex_results.read_stx_file()
    hqex_results.read_prx_file()