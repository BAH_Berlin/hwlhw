# -*- coding: utf-8 -*-
"""
Monte-Carlo and manual simulation of synthetic flood events.

Created on Sun May 30 16:50:13 2021
@author: ruben.mueller
"""
import os
import scipy
import datetime
import numpy as np
import pandas as pd
from hwlhw.hwlhw_model_tb import TB_MODELMAP
from hwlhw.hwlhw_utils import (cdf_array, cdf_array2)
from hwlhw.hwlhw_utils import update_progress

from plotly.subplots import make_subplots
import plotly.graph_objects as go

class HQ_simulation:
    """Class for simulation of synthetic flood events."""

    def __init__(self, distribution):
        """Simulate synthetic flood events.

        Parameters
        ----------
        distribution : Distribution
            The class with the fitted distributions."""
        self._distribution = distribution
        self.hq_events = {}
        self.hq_events_m = {}
        self.simulations = 1000
        self.simulations_m = 1


    def checkpar(self, par, seas, p, distr_name, est):
        if par == "m_an":
            mx = [1, 10]
        elif par == "m_ab":
            mx = [0.05, 0.5]
        elif par == "t_a":
            #if not "t_a" in self._distribution.parameter_stats[seas[0]]:
            #    par = "t_A"
            mx = [1,
                  self._distribution.parameter_stats[seas[0]][par]['max']*2]
        #elif par == 't_A':
        ##    mx = [1,
        #          self._distribution.parameter_stats[seas[0]][par]['max']*1.5]
        elif par == "q_s":
            mx = [0,
                  p.loc[(distr_name, est), :].values[-1]]
                  #self._distributionparameter_stats[seas[0]][par]['max']*2]
        elif par == "t_p":
            mx = [1,
                  self._distribution.parameter_stats[seas[0]][par]['max']*1.5]
        return mx


    def mc_model(self, m_fit, map_dict, simulations=1000, dp_factor=3, max_tries=10) -> None:
        """Generate synthetic events with the Monte-Carlo method.

        Parameters
        ----------
        m_fit : Model_fitter
            Class for event simulation.
        map_dict : dict
            set a distribution for a parameter
            >>>
            >>> {'m_an': ['P3', 'MLM'], 'm_ab': ['AE', 'MM'],
            >>>  't_s': ['LN3', 'MM'], 'q_s': ['E1', 'MLM']
            >>> }
        simulations : int, optional
            Number of events to generate. The default is 1000.
        dp_factor : int
            The period for the descending part of the event is int(t_a * dp_factor)
        max_tries : int
            maximum number of tries to find parameters for an event"""

        self.simulations = simulations
        self.hq_events = {}
        [self.hq_events.update({'season_{}'.format(i): {}}) for i, seas in enumerate(self._distribution.modelvar_dist)]

        for seas in self._distribution.modelvar_dist.items():
            model = m_fit.models[int(seas[0][-1])] #TB_MODELMAP[seas[1]['modelname']]['model']()
            if not model.name == seas[1]['modelname']:
                raise TypeError("Error: Versionen passen nicht zusammen. Sehe unterschiedliche Modelle!")
            parameternames1 = TB_MODELMAP[seas[1]['modelname']]['parameter']
            parameternames2 = TB_MODELMAP[seas[1]['modelname']]['parameter_fixed']
            season_number = int(seas[0][-1])
            if season_number == 1:
                pass

            correct = 0
            max_len = 0
            try_number = 0
            collection = []
            while correct < self.simulations and try_number < self.simulations * max_tries:
                try_number += 1

                hqs = np.zeros(len(parameternames1))

                for j, par in enumerate(parameternames1):
                    p = self._distribution.modelvar_dist[seas[0]]['p'][par].df_prx
                    fkt_m = 5 if par == 'q_s' else 1
                    mx = self._distribution.parameter_stats[seas[0]][par]['max'] * fkt_m
                    distr_name = map_dict[par][0]
                    est = map_dict[par][1]

                    vvec = cdf_array(p, distr_name, est, mx, resolution=300)
                    x = np.random.rand()
                    val = np.interp(x, vvec[0,:], vvec[2,:])
                    if np.isnan(val): val = np.nanmin(vvec[2,:])
                    if np.isinf(val): val = np.nanmax(vvec[2,:])
                    hqs[j] = val

                event_loc = {'start': datetime.datetime(2020, 1, 1),
                         'end': datetime.datetime(2020, 1, 1) \
                                + dp_factor*m_fit.s_ev.seasonal_stats[season_number]['t_a'],
                         'qo': m_fit.s_ev.qo,
                         'period': m_fit.s_ev.period,
                         'Qbase': 0,
                         'freq': m_fit.s_ev.freq,
                         'ta': None,
                         'Qs': None}

                if parameternames2:
                    for j, par in enumerate(parameternames2):
                        p = self._distribution.modelvar_dist[seas[0]]['p'][par].df_prx
                        fkt_m = 5 if par == 'q_s' else 1
                        mx = self._distribution.parameter_stats[seas[0]][par]['max'] * fkt_m
                        distr_name = map_dict[par][0]
                        est = map_dict[par][1]

                        vvec = cdf_array(p, distr_name, est, mx, resolution=300)
                        x = np.random.rand()
                        hqf = np.interp(x, vvec[0,:], vvec[2,:])
                        if np.isnan(val): hqf = np.nanmin(vvec[2,:])
                        if np.isinf(val): hqf = np.nanmax(vvec[2,:])

                        if par == 't_a':
                            event_loc['ta'] = hqf
                        else:
                            event_loc['Qs'] = hqf


                res = model.run(hqs, event_loc, None, plot=False, opt=False, ax=None)

                #print(try_number, correct)
                if not res == 9999:


                    correct += 1
                    collection.append(res)
                    update_progress(correct/self.simulations)
                    new_len = len(res['timeseries'])
                    if new_len > max_len:
                        max_len = new_len



            print ('ran', try_number, 'of', self.simulations)
            self.hq_events[seas[0]]['events'] = collection
            self.hq_events[seas[0]]['max_len'] = max_len
            self.hq_events[seas[0]]['modelname'] = seas[1]['modelname']


    def mc_model2(self, m_fit, map_dict, simulations=1000, dp_factor=3, max_tries=10, signal=None) -> None:
        """Generate synthetic events with the Monte-Carlo method.

        Parameters
        ----------
        m_fit : Model_fitter
            Class for event simulation.
        map_dict : dict
            set a distribution for a parameter
            >>>
            >>> {'m_an': ['P3', 'MLM'], 'm_ab': ['AE', 'MM'],
            >>>  't_s': ['LN3', 'MM'], 'q_s': ['E1', 'MLM']
            >>> }
        simulations : int, optional
            Number of events to generate. The default is 1000.
        dp_factor : int
            The period for the descending part of the event is int(t_a * dp_factor)
        max_tries : int
            maximum number of tries to find parameters for an event"""

        self.simulations = simulations
        self.hq_events = {}
        [self.hq_events.update({'season_{}'.format(i): {}}) for i, seas in enumerate(self._distribution.modelvar_dist)]
        c_tot = 0
        for seas in self._distribution.modelvar_dist.items():
            model = m_fit.models[int(seas[0][-1])] #TB_MODELMAP[seas[1]['modelname']]['model']()
            parameternames1 = TB_MODELMAP[model.name]['parameter']#TB_MODELMAP[seas[1]['modelname']]['parameter']
            parameternames2 = TB_MODELMAP[model.name]['parameter_fixed']#TB_MODELMAP[seas[1]['modelname']]['parameter_fixed']
            season_number = int(seas[0][-1])
            if season_number == 1:
                pass

            correct = 0
            max_len = 0
            try_number = 0
            collection = []
            while correct < self.simulations and try_number < self.simulations * max_tries:

                try_number += 1

                hqs = np.zeros(len(parameternames1))

                for j, par in enumerate(parameternames1):

                    p = self._distribution.stx_dist[seas[0]]['p'][par].df_stx
                    distr_name = map_dict[seas[0]][par][0]
                    est = map_dict[seas[0]][par][1]

                    vvec = cdf_array2(p, distr_name, est)
                    x = np.min((np.random.rand(), 0.999))
                    val = np.interp(x, vvec[0,:], vvec[1,:])
                    bound = self.checkpar(par, seas, p, distr_name, est)
                    val = np.max((np.min((val, bound[1])), bound[0]))
                    hqs[j] = val

                event_loc = {'start': datetime.datetime(2020, 1, 1),
                         'end': datetime.datetime(2020, 1, 1) \
                                + dp_factor*m_fit.s_ev.seasonal_stats[season_number]['t_a'],
                         'qo': m_fit.s_ev.qo,
                         'period': m_fit.s_ev.period,
                         'Qbase': 0,
                         'freq': m_fit.s_ev.freq,
                         'ta': None,
                         'Qs': None}

                if parameternames2:
                    for j, par in enumerate(parameternames2):
                        p = self._distribution.stx_dist[seas[0]]['p'][par].df_stx
                        distr_name = map_dict[seas[0]][par][0]
                        est = map_dict[seas[0]][par][1]

                        vvec = cdf_array2(p, distr_name, est)
                        x = np.min((np.random.rand(), 0.999))
                        hqf = np.interp(x, vvec[0,:], vvec[1,:])
                        bound = self.checkpar(par, seas, p, distr_name, est)
                        hqf = np.max((np.min((hqf, bound[1])), bound[0]))

                        if np.isnan(val): hqf = np.nanmin(vvec[1, :])
                        if np.isinf(val): hqf = np.nanmax(vvec[1, :])

                        if par == 't_a':
                            event_loc['ta'] = hqf
                        else:
                            event_loc['Qs'] = hqf


                res = model.run(hqs, event_loc, None, plot=False, opt=False, ax=None)

                if not res == 9999:

                    if np.nanmax(res['timeseries'].values) >= m_fit.s_ev.qo*0.75 \
                    or np.nanmax(res['timeseries'].values) == 0:
                        continue

                    correct += 1
                    if not signal is None:
                        c_tot += 1
                        if not c_tot % 5:
                            msg = [(c_tot)/self.simulations/len(self._distribution.modelvar_dist)*100, "{}".format(c_tot)]
                            #print(msg)
                            signal.emit(msg)
                    else:
                        update_progress(correct/self.simulations)
                    collection.append(res)
                    new_len = len(res['timeseries'])
                    if new_len > max_len:
                        max_len = new_len

            print ('ran', try_number, 'of', self.simulations)
            self.hq_events[seas[0]]['events'] = collection
            self.hq_events[seas[0]]['max_len'] = max_len
            self.hq_events[seas[0]]['modelname'] = seas[1]['modelname']




    def mean_event_method(self, m_fit, qs_base, dp_factor=3, max_tries=100, return_res=False):
        """Generate snthetic events with the manuel method.

        Parameters
        ----------
        m_fit : Model_fitter
            Class for event simulation.
        qs_base : dict
            peak flow for the base scenario and 3 other simulations.
        dp_factor : int
            The period for the descending part of the event is int(t_a * dp_factor)
        max_tries : int
            maximum number of tries to find parameters for an event"""

        self.hq_events_m = {}
        [self.hq_events_m.update({'season_{}'.format(i): {}}) for i, seas in enumerate(self._distribution.modelvar_dist)]

        ### check if necessary steps have been taken
        if not self._distribution.parameter_stats:
            self._distribution.build_distribution_structure(m_fit)
            self._distribution.collect_parameters(m_fit)
            self._distribution.parameter_statistics()

        self.mean_m_p = {}

        ###
        def o_func(x, medpar, event_loc, target_vol, parameternames1, retres):
            if 't_a' in parameternames1:
                med_par[parameternames1.index('t_a')] = x
            else:
                event_loc['ta'] = x
            res = model.run(med_par, event_loc, None,
                            plot=False, opt=False, ax=None)
            if not retres:
                if not res == 9999:
                    y = abs(res['vol_sim'] - target_vol)
                else:
                    y = 9999
                return y
            else:
                return res

        for seas in self._distribution.modelvar_dist.items():
            model = m_fit.models[int(seas[0][-1])]
            parameternames1 = TB_MODELMAP[seas[1]['modelname']]['parameter']
            parameternames2 = TB_MODELMAP[seas[1]['modelname']]['parameter_fixed']

            ### What to use for the primary estimator
            estimator = 'mean' # 'median'
            if not 't_a' in self._distribution.parameter_stats[seas[0]] \
            and 't_A' in self._distribution.parameter_stats[seas[0]]:
                self._distribution.parameter_stats[seas[0]]['t_a'] = self._distribution.parameter_stats[seas[0]]['t_A']
                
                    
            med_par = [self._distribution.parameter_stats[seas[0]][x][estimator] for x in parameternames1]
            self.mean_m_p.update({seas[0]: {}})

            season_number = int(seas[0][-1])
            event_loc = {'start': datetime.datetime(2020, 1, 1),
                         'end': datetime.datetime(2020, 1, 1) \
                                + dp_factor * m_fit.s_ev.seasonal_stats[season_number]['t_a'],
                         'qo': m_fit.s_ev.qo,
                         'period': m_fit.s_ev.period,
                         'Qbase': 0,
                         'freq': m_fit.s_ev.freq,
                         'ta': None}

            if 't_a' in parameternames2:
                med_par2 = self._distribution.parameter_stats[seas[0]]['t_a'][estimator]
                event_loc['ta'] = med_par2
                px1 = med_par2
            else:
                px1 = med_par[parameternames1.index('t_a')]

            if 'q_s' in parameternames2:
                event_loc['Qs'] = qs_base[seas[0]]['HQ*']
            else:
                med_par[parameternames1.index('q_s')] = qs_base[seas[0]]['HQ*']

            median_variant = model.run(med_par, event_loc, None, plot=False, opt=False, ax=None)

            print('V:', median_variant['vol_sim']/1000000*60*15, 't_a:', px1, 'Q:', qs_base)

            self.simulations_m = len(qs_base)

            self.mean_m_p[seas[0]].update({qs_base[seas[0]]['HQ*']: """Peak {};\nVolume {};\nt_a {};\nmedian parameters {});""".format(qs_base[seas[0]]['HQ*'],
                                           median_variant['vol_sim'],
                                           px1,
                                           median_variant['parameter'],
                                           )})

            max_len = 0
            collection = {}

            target_vol = median_variant['vol_sim']

            for qcsn in ['HQ1', 'HQ2', 'HQ3']:
                qcs = qs_base[seas[0]][qcsn]
                if 'q_s' in parameternames2:
                    event_loc['Qs'] = qcs
                else:
                    med_par[parameternames1.index('q_s')] = qcs


                bnd_t = int(dp_factor / 2 \
                            * m_fit.s_ev.seasonal_stats[season_number]['t_a'].total_seconds() \
                            / m_fit.s_ev.period)
                res = scipy.optimize.dual_annealing(o_func,
                                                    x0=[px1],
                                                    maxiter=1000,
                                                    bounds=[[1, bnd_t]],
                                                    args=(med_par, event_loc,
                                                          target_vol,
                                                          parameternames1,
                                                          False))

                vols = o_func(res.x, med_par, event_loc,
                              target_vol,
                              parameternames1,
                              True)
                self.mean_m_p[seas[0]].update({qcs: """Volume {}\nt_a {}\nmedian parameters {} (Names:{});\nfixed parameters {} (Names:{})""".format(vols['vol_sim'],
                                                res.x,
                                                vols['parameter'],
                                                parameternames1,
                                                vols['parameter_fixed'],
                                                parameternames2,
                                                )})
                collection[qcs] = vols
                new_len = len(vols['timeseries'])
                if new_len > max_len: max_len = new_len
                print('V', vols['vol_sim']/1000000*60*15, 't_a', res.x)
                print('-----------')

                self.hq_events_m[seas[0]]['events'] = collection
                self.hq_events_m[seas[0]]['max_len'] = max_len
                self.hq_events_m[seas[0]]['modelname'] = seas[1]['modelname']
            self.hq_events_m[seas[0]]['med_variant'] = median_variant
            self.hq_events_m[seas[0]]['med_variant']['qs'] = qs_base
        return self.hq_events_m


    def plot_mean_events(self, figure=None, hq_events_m=None):
        """Plot the events created with the manual method."""
        fig = make_subplots(rows=1, cols=max(len(self._distribution.modelvar_dist), 1))
        
        if hq_events_m is None:
           hq_events_m = self.hq_events_m

        
        for seas_key in self._distribution.modelvar_dist:
            s = int(seas_key[-1])+1
            for ev in hq_events_m[seas_key]['events'].items():
                fig.add_trace(go.Scatter(y=ev[1]['timeseries'].iloc[:,0],
                                         x=ev[1]['timeseries'].index,
                                         name=ev[0]), row=1, col=s)
            fig.add_trace(go.Scatter(y=hq_events_m[seas_key]['med_variant']['timeseries'].iloc[:, 0],
                                     x=hq_events_m[seas_key]['med_variant']['timeseries'].index,
                                     name='*Qs {:.1f}'.format(hq_events_m[seas_key]['med_variant']['qs'][seas_key]['HQ*'])),
                          row=1, col=s)

        fig.update_layout(
            title="Ereignisse aus mittlerer Methode",
            xaxis_title="Datum/Zeit",
            yaxis_title= "Q (m<sup>3</sup>s<sup>-1</sup>)",#r"$\mathrm{Q}\quad (\mathrm{m^{3}s^{-1}})$",
            font=dict(
                family="Courier New, monospace",
                size=12
            )
        )
        if figure is None:
            fig.show()
        else:
            return fig


    def plot_overview_mc(self, m_fit, figure=None):
        """Plot the events created with the Monte-Carlo method.

        Parameters
        ----------
        m_fit : Model_fitter
            Class for event simulation."""

        for m_fit.seas in self.hq_events.items():
            fig = make_subplots(rows=1, cols=2)
            for i, hq in enumerate(m_fit.seas[1]['events']):
                fig.add_trace(go.Scatter(y=hq['timeseries'].values[:, 0]), row=1, col=1)
            for event in m_fit.s_ev.events.items():
                if event[1]['season'] == int(m_fit.seas[0][-1]):
                    fig.add_trace(go.Scatter(y=m_fit.s_ev.timeseries.series_gauge.loc[event[1]["start"]:event[1]["end"], "Q"]),
                                  row=1, col=2)

            fig.update_layout(
                title="Ereignisse aus Monte-Carlo",
                xaxis_title="Zeitschritte",
                xaxis2_title="Zeitschritte",
                yaxis_title="Q (m<sup>3</sup>s<sup>-1</sup>)", #r"$\mathrm{Q}\quad (\mathrm{m^{3}s^{-1}})$",
                yaxis2_title="Q (m<sup>3</sup>s<sup>-1</sup>)", #r"$\mathrm{Q}\quad (\mathrm{m^{3}s^{-1}})$",
                font=dict(
                    family="Courier New, monospace",
                    size=12
                )
            )
            fig.update_layout(showlegend=False)
            if figure is None:
                fig.show()
            else:
                return fig


    def write_mc(self, m_fit, workspace, gaugename):
        """Write an text file with the results for the Monte-Carlo method.

        Parameters
        ----------
        m_fit : Model_fitter
            Class for event simulation.
        workspace : str
            The target folder
        gaugename : str
            Name of the gauge as part of the filename."""

        for m_fit.seas in self.hq_events.items():
            res_array = np.zeros((m_fit.seas[1]['max_len'], self.simulations))
            for i, hq in enumerate(m_fit.seas[1]['events']):
                res_array[:len(hq['timeseries']), i] = hq['timeseries'].values[:, 0]


            df = pd.DataFrame(res_array,
                    index=pd.date_range(start=datetime.datetime(2000, 1, 1, 0, 0),
                    end=datetime.datetime(2000, 1, 1, 0, 0) + datetime.timedelta(minutes=len(res_array)*15-1),
                    freq='15T'))
            df.to_csv(os.path.join(workspace, gaugename + '_season_{}_MC'.format(m_fit.seas[0]) + '.csv'),
                      header= ['HQ{}'.format(x) for x in df.columns.values], index_label = 'time', sep=';')


    def write_mean_method(self, m_fit, workspace, gaugename, hqx='', hq_events_m=None):
        """Write an text file with the results for the manual method.

        Parameters
        ----------
        s_ev : Model_fitter
            Class for seperation of events.
        workspace : str
            The target folder
        gaugename : str
            Name of the gauge as part of the filename."""
            
        if hq_events_m is None:
            hq_events_m = self.hq_events_m

        for m_fit.seas in self.hq_events_m.items():

            res_array = np.zeros((m_fit.seas[1]['max_len'], 4))
            res_array[:len(m_fit.seas[1]['med_variant']['timeseries']), 0] = m_fit.seas[1]['med_variant']['timeseries'].values[:, 0]
            header = ["*Qs{}".format(m_fit.seas[1]['med_variant']['qs'][m_fit.seas[0]]["HQ*"])]

            for i, hq in enumerate(m_fit.seas[1]['events'].items()):
                res_array[:len(hq[1]['timeseries']), i+1] = hq[1]['timeseries'].values[:, 0]
                header.append("Qs{}".format(hq[0]))

            df = pd.DataFrame(res_array,
                    index=pd.date_range(start=datetime.datetime(2000, 1, 1, 0, 0),
                    end=datetime.datetime(2000, 1, 1, 0, 0) + datetime.timedelta(minutes=len(res_array)*15-1),
                    freq='15T'))
            df.to_csv(os.path.join(workspace, gaugename + '_' + hqx + '_season_{}_MM'.format(m_fit.seas[0]) + '.csv'),
                      header= header, index_label = 'time', sep=';')
