# -*- coding: utf-8 -*-
"""
Settings and mapper for the hwlhw package.
Do not change unless you know what, when, how and why...

Created on Sun May 30 16:32:25 2021
@author: ruben.mueller
"""

import numpy as np
from scipy import stats
from hwlhw.hwlhw_criteria import (rmse, KlingGuptaEfficiency,
    relNSE, dValue, R2)
from hwlhw.hwlhw_distributions import *

DATEFORMAT_F = {'hour': '%d.%m.%Y %H:%M',
                'minute': '%d.%m.%Y %H:%M:%S',
                'day': "%d.%m.%Y"}

# Distributions: MAP the scipy distributions to the HQEX-names. -> pdf for paramater synthesis
DIST_MAP = {"E1": gumbel_e1,
            "AE": extrem_ae,
            "LN3": lognorm_ln3,
            "P3": stats.pearson3,
            "WB3": weibull_wb3,
            "LP3": lambda x, a, b, c: scipy.stats.pearson3(log(x), b, c),
            "ROV": rossi}

# Distributions: MAP the scipy distributions to the HQEX-names. -> cdf for plotting
DISTF_MAP = {"E1": gumbel_e1F,
            "AE": extrem_aeF,
            "LN3": lognorm_ln3F,
            "P3": pearson_3F,
            "WB3": weibull_wb3F,
            "LP3": log_pearson_3F,
            "ROV": rossiF}

# Distributions: MAP the HQEX-parameter ordering to the scipy parameter ordering
DIST_MAP_PAR = {"E1": [0, 1],
                "AE": [2, 0, 1],
                "LN3": [2, 0, 1],
                "P3": [2, 0, 1],
                "WB3": [2, 0, 1],
                "LP3": [2, 0, 1],
                "ROV": [0, 2, 1, 3]}
DIST_MAP_PARN = {"E1": ["p1", "p2"],
                "AE": ["p3", "p1", "p2"],
                "LN3": ["p3", "p1", "p2"],
                "P3": ["p3", "p1", "p2"],
                "WB3": ["p3", "p1", "p2"],
                "LP3": ["p3", "p1", "p2"],
                "ROV": ["p1", "p3", "p2", "p4"],}
ESTIMATORS = ['MM', 'MLM', 'WGM']

# The assumed timestep (we focus only on the WISKi-15min Data yet)
TIMESTEP = 15/1440

TB_MODELMAP = {"BenderJensen_full": {'model': None,
                                     'parameter': ['q_s', 't_a',
                                                   'm_an', 'm_ab', 't_p'],
                                     'parameter_fixed': []},
               "BenderJensen_fixed_noTp": {'model': None,
                                           'parameter': ['q_s', 'm_an',
                                                         'm_ab', 't_a'],
                                      'parameter_fixed': []},
               "BenderJensen_fixed_tA": {'modll': None,
                                         'parameter': ['q_s', 'm_an',
                                                       'm_ab', 't_p'],
                                         'parameter_fixed': ['t_a']},
               "BenderJensen_form": {'model': None,
                                     'parameter': ['m_an', 'm_ab'],
                                      'parameter_fixed': ['t_a', 'q_s']},
               "Triangle_rmse": {'model': None,
                                 'parameter': ['q_s', 't_p', 'm_an',
                                               'm_ab'],
                                 'parameter_fixed': []}}


MAPPER = {'vol': {'sim': 'vol_sim',
                  'obs': 'vol_obs',
                  'func_sim': lambda x, y, pars: x[y]/1000000*60*15,
                  'func_obs': lambda x, y, pars: x[y]/1000000*60*15,
                  #'label': r'$\mathrm{vol\ (hm^3)}$'
                  'label': 'vol (hm<sup>3</sup>)'
                  },
         't_a': {'sim': 'ta',
                 'obs': 'ta',
                 'func_sim': lambda x, y, pars: np.argmax(x['timeseries'].values) / 4,
                 'func_obs': lambda x, y, pars: x[y].total_seconds() / 3600,
                 'label': 't<sub>a</sub> (h)' #r'$\mathrm{t_{a}\ (h)}$'
                 },
         'q_s': {'sim': 'q_s',
                 'obs': 'q_s',
                 'func_sim': lambda x, y, pars: x['parameter'][pars.index(y)],
                 'func_simB': lambda x, y, pars: x['parameter_fixed'][pars.index(y)],
                 'func_obs': lambda x, y, pars: x['parameter'][pars.index(y)],
                 'func_obsB': lambda x, y, pars: x['parameter_fixed'][pars.index(y)],
                 'label': 'q<sub>s</sub> (m<sup>3</sub>s<sup>-1</sub>)'#r'$\mathrm{q_{s}\ (m^{3}s^{-1})}$'
                 },
         'm_an': {'sim': 'm_an',
                  'obs': 'm_an',
                  'func_sim': lambda x, y, pars: x['parameter'][pars.index(y)],
                  'func_obs': lambda x, y, pars: x['parameter'][pars.index(y)],
                  'label': 'm<sub>an</sub> (-)' #r'$\mathrm{m_{an}\ (-)}$'
                  },
         'm_ab': {'sim': 'm_ab',
                  'obs': 'm_ab',
                  'func_sim': lambda x, y, pars: x['parameter'][pars.index(y)],
                  'func_obs': lambda x, y, pars: x['parameter'][pars.index(y)],
                  'label': 'm<sub>ab</sub> (-)'#  r'$\mathrm{m_{ab\ (-)}}$'
                  },
         't_p': {'sim': 't_p',
                  'obs': 't_p',
                  'func_sim': lambda x, y, pars: x['parameter'][pars.index(y)],
                  'func_obs': lambda x, y, pars: x['parameter'][pars.index(y)],
                  'label': 't<sub>p</sub> (-)' #r'$\mathrm{t_{p\ (-)}}$'
                  },
         'crit': {'sim': 'KGE (-)',
                  'obs': '',
                  'func_sim': lambda x, y, pars: np.nan,
                  'func_obs': lambda x, y, pars: x['fitness'][pars.index(y)]*-1,
                  'label': 'KGE (-)' #r'$\mathrm{KGE\ (-)}$'
                  }
 }


FFTYPES= {'RMSE': rmse, 'KlingGuptaEfficiency': KlingGuptaEfficiency,
          'relNSE': relNSE, 'dValue': dValue, 'R2': R2}
