# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 18:08:25 2021

@author: ruben.mueller
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 20:05:49 2021

@author: ruben.mueller
"""

import numpy as np
import scipy


def gumbel_e1(x, c, d): # OK
    #if d <= 0: raise ValueError('d <= 0')
    fx = 1/d * np.exp(-1*(x-c)/d) * np.exp(-1*np.exp(-1*(x-c)/d))
    return fx

def gumbel_e1F(x, c, d): # OK
    #if d <= 0: raise ValueError('d <= 0')
    fx = np.exp(-1*np.exp(-1*(x-c)/d))
    return fx


def extrem_ae(x, a, c, d): # OK
   # if a < 0 and np.min(x) < c-d/-a: raise ValueError('a < 0 --> np.min(x) < c-d/-a')
   # if a > 0 and np.max(x) > c+d/a: raise ValueError('a > 0 --> np.max(x) > c+d/a')
    fx = 1/d * np.power((1 - a*(x-c)/d), 1/a-1) + np.exp(-1 * np.power((1 - a*(x-c)/d), 1/a))
    return fx

def extrem_aeF(x, a, c, d): # OK
   # if a < 0 and np.min(x) < c-d/-a: raise ValueError('a < 0 --> np.min(x) < c-d/-a')
    #if a > 0 and np.max(x) > c+d/a: raise ValueError('a > 0 --> np.max(x) > c+d/a')
    fx = np.exp(-1 * np.power((1 - a*(x-c)/d), 1/a))
    return fx


def rossi(x, c1, c2, d1, d2):
    #if d1 <= 0: raise ValueError('d1<= 0')
    #if d2 <= 0: raise ValueError('d2<= 0')
    t1 = 1/d1 * np.exp(-(x-c1)/d1) * np.exp( -1*np.exp(-(x-c1)/d1) ) * np.exp( -1*np.exp(-(x-c2)/d2) )
    t2 = 1/d2 * np.exp(-(x-c2)/d2) * np.exp( -1*np.exp(-(x-c2)/d2) ) * np.exp( -1*np.exp(-(x-c1)/d1) )
    fx = t1 + t2
    return fx

def rossiF(x, c1, c2, d1, d2): ## 0,2,1,3
    fx = np.exp( -1*np.exp(-(x-c2)/d2) ) * np.exp( -1*np.exp(-(x-c1)/d1) )
    return fx


def lognorm_ln3(x, c, d, e):
    #if d <= 0: raise ValueError('d<= 0')
    #if np.min(x) < c: raise ValueError('np.min(x) < c')
    fx = 1/(np.sqrt(2*np.pi)*(x-c)*d) * np.exp(-1/2 * np.sqrt(np.log(x-c)-e)/d)
    return fx

def lognorm_ln3F(x, a, c, d):  # OK
    #fx = scipy.stats.norm.cdf(np.log(x), d, loc=c, scale=a)
    x1 = np.exp(np.log(x-c)-d)
    fx = scipy.stats.lognorm.cdf(x1, a)
    return fx


def pearson_3(x, a, c, d):
    #if a <= 0: raise ValueError('a<= 0')
    #if d == 0: raise ValueError('d == 0')
    #if d < 0 and np.min(x) < c: raise ValueError('d < 0 --> np.min(x) < c')
    #if d > 0 and np.max(x) > c: raise ValueError('d > 0 --> np.max(x) > c')
    if a == 0:
        pdfcor = scipy.stats.norm.pdf((x-c)/d) / d
    else:
        alpha = 4 / np.power(a, 2)
        beta = 0.5 * c * np.abs(a)
        xi = c - 2 * d / a
        if a > 0:
            y = x - xi
            pdfcor = scipy.stats.gamma.pdf(y / beta, alpha) / beta
        else:
            y = xi - x
            pdfcor = scipy.stats.gamma.pdf(y / beta, alpha) / beta
    return pdfcor


def pearson_3F(x, a, c, d): # OK
    return scipy.stats.gamma.cdf(x, a, c, d)


def log_pearson_3F(x, a, c, d): # OK
    # if a <= 0: raise ValueError('a<= 0')
    # if d == 0: raise ValueError('d == 0')
    # if d < 0 and np.min(x) < np.exp(c): raise ValueError('d < 0 --> np.min(x) < np.exp(c)')
    # if d > 0 and np.max(x) > np.exp(c): raise ValueError('d > 0 --> np.max(x) > np.exp(c)')
    #x1 = np.exp(np.log(x-c)-d)

    return scipy.stats.gamma.cdf(np.log(x), a, c, d)



def weibull_wb3(x, a, c, d):
    #if a <= 0: raise ValueError('a<= 0')
    #if d <= 0: raise ValueError('d<= 0')
    fx = a/d * np.power((x-c)/d, a-1) * np.exp(np.power(-1*(x-c)/d, a))
    return fx

def weibull_wb3F(x, a, c, d): # OK
    #if a <= 0: raise ValueError('a<= 0')
    #if d <= 0: raise ValueError('d<= 0')
    fx = 1 - np.exp(-1*np.power((x-c)/d, a))
    return fx
