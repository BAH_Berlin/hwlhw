# -*- coding: utf-8 -*-
"""
Fit a distribution to the form parameter sample.
Currently only the Pearson3 distribution is supported.

Created on Sun May 30 16:50:13 2021
@author: ruben.mueller
"""
import os
from copy import deepcopy
import numpy as np
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from hwlhw.hwlhw_hqex_import import Hqex_dist_para
from hwlhw.hwlhw_separate_events import Separate_events
from hwlhw.hwlhw_model_fit import Model_fitter
from hwlhw.hwlhw_settings import (DIST_MAP, DIST_MAP_PARN,
                                  DISTF_MAP, DIST_MAP_PAR,
                                  ESTIMATORS)
from hwlhw.hwlhw_model_tb import TB_MODELMAP

class Distribution:
    """Class for distibutions, statistics and analytics."""

    def __init__(self, dist='P3'):
        """Distributions for parameters and flows

        Parameters
        ----------
        dist : str
            The chosen distirbution (P3 for base version only)"""
        self.__selected_parameter = None
        self.__dist = DIST_MAP[dist]
        self.__dist_param = None
        self.modelvar_dist = {}
        self.stx_dist = {}
        self.parameter_season = {}
        self.parameter_stats = {}
        self._dist_fist = {}
        self.hqt = []

    @property
    def chosen_dist(self):
        """Return the chosen distribution"""
        return self.__dist


    def select_distribution(self, dist_param, dist, est) -> None:
        """Select a distribution from the HQ-EX import.

        Parameters
        ----------
        dist_param : Hqex_dist_para
            the HQ-EX import class
        dist : str
            distribution name (P3 in base version only)
        est : str
            the estimator type (MM, MLM, WGM)"""
        if not isinstance(dist_param, Hqex_dist_para):
            raise TypeError("dist_param must be type Hqex_dist_para.")
        self.__dist_param = dist_param
        print("Selected distribution {}, estimated with {} and parameter {}, {} & {}".format(dist,
            est, *self.__dist_param.df_prx.loc[(dist, est), [4, 5, 6]]))
        self.__selected_dist = (dist, est)
        ### ordering HQ-EX and Scipy
        self.__selected_parameter = self.__dist_param.df_prx.loc[(dist, est), DIST_MAP_PAR[dist]].values


    def build_distribution_structure(self, evclass) -> None:
        """Get all fitted parameters from the events.

        Parameters
        ----------
        evclass : Separate_events or Model_fitter
            Class for event seperation or model fitting."""
        if  isinstance(evclass, Separate_events):
            s_ev = evclass
        elif  isinstance(evclass, Model_fitter):
            s_ev = evclass.s_ev
        else:
            raise TypeError("s_ev must be of type Separate_events or Model_fitter")

        season = {}
        dist_fist = {}
        [season.update({'season_{}'.format(i): {}}) for i, seas in enumerate(s_ev.seasons)]
        [dist_fist.update({'season_{}'.format(i): {}}) for i, seas in enumerate(s_ev.seasons)]
        ### get the parameter
        for i, s in enumerate(s_ev.seasons):
            for event in s_ev.events.items():
                if event[1]['start'].month in s:
                    season['season_{}'.format(i)]['modelname'] = event[1]['modelname']
                    dist_fist['season_{}'.format(i)]['modelname'] = event[1]['modelname']
                    pars = TB_MODELMAP[event[1]['modelname']]['parameter']
                    pars2 = TB_MODELMAP[event[1]['modelname']]['parameter_fixed']

                    ### model parameter list
                    season['season_{}'.format(i)]['p'] = {}
                    ### fitted distribution list
                    dist_fist['season_{}'.format(i)]['p'] = {}
                    [season['season_{}'.format(i)]['p'].update({p: []}) for p in pars]
                    [dist_fist['season_{}'.format(i)]['p'].update({p: []}) for p in pars]

                    [season['season_{}'.format(i)]['p'].update({p: []}) for p in pars2]
                    [dist_fist['season_{}'.format(i)]['p'].update({p: []}) for p in pars2]
                    break
        self._dist_fist = dist_fist
        self._dist_fist2 = deepcopy(dist_fist)
        self._dist_fit = deepcopy(dist_fist)
        self.parameter_season = season


    def collect_parameters(self, evclass) -> None:
        """Collect the parameters from all events.

        Parameters
        ----------
        evclass : Separate_events or Model_fitter
            Class for event seperation or model fitting."""
        if  isinstance(evclass, Separate_events):
            s_ev = evclass
        elif  isinstance(evclass, Model_fitter):
            s_ev = evclass.s_ev
        else:
            raise TypeError("s_ev must be of type Separate_events or Model_fitter")

        for event in s_ev.events.items():
            parameternames = TB_MODELMAP[event[1]['modelname']]['parameter']
            seas = event[1]['season']
            for i, p in enumerate(parameternames):
                self.parameter_season['season_{}'.format(seas)]['p'][p].append(event[1]['parameter'][i])

            parameternames2 = TB_MODELMAP[event[1]['modelname']]['parameter_fixed']
            for i, p in enumerate(parameternames2):
                self.parameter_season['season_{}'.format(seas)]['p'][p].append(event[1]['parameter_fixed'][i])


    def fit_distribution_model(self) -> None:
        """Fit a setected distribution to the parameters."""

        for seas_key, seas_val in self.parameter_season.items():
             if seas_key == 'modelname':
                 continue
             for par_key, par_val in seas_val['p'].items():
                 self._dist_fist[seas_key]['p'][par_key] \
                     = self.__dist.fit(self.parameter_season[seas_key]['p'][par_key])
        self.modelvar_dist = self._dist_fist

        print('fitting done.')


    def import_hqex(self, workspace, import_dict) -> None:
        """Import the HQEX parameter output file.

        Parameters
        ----------
        workspace : str
            the folder
        imput_dict : dict
            describes which files to import.

        Example
        -------
        >>> import_dict = [
        >>>     ['season_0', 'q_s', HQ-EX Export1'],
        >>>     ['season_0', 't_A', 'HQ-EX Export2'],
        >>>     ['season_0', 'm_an', 'HQ-EX Export3'],
        >>>     ['season_0', 'm_ab', 'HQ-EX Export4'],
        >>>
        >>>     ['season_1', 'q_s', 'HQ-EX Export5'],
        >>>     ['season_1', 't_A', 'HQ-EX Export6'],
        >>>     ['season_1', 'm_an', 'HQ-EX Export7'],
        >>>     ['season_1', 'm_ab', 'HQ-EX Export8'],
        >>>     ['season_1', 't_p', 'HQ-EX Export9']
        >>> ]"""
        for pset in import_dict:
            hqex = Hqex_dist_para()
            hqex.folder = workspace
            hqex.filename = pset[2]
            hqex.read_prx_file()
            self._dist_fist[pset[0]]['p'][pset[1]] = hqex # NEW

            gk = (1 - hqex.df_prx.iloc[:, -1] + hqex.df_prx.iloc[:, -2]).sort_values()
            
            self._dist_fit[pset[0]]['p'][pset[1]] = gk

        self.modelvar_dist = self._dist_fist
        self.modelvar_fit = self._dist_fit

    def import_stx(self, workspace, import_dict) -> None:
        """Import the HQEX parameter output file.

        Parameters
        ----------
        workspace : str
            the folder
        imput_dict : dict
            describes which files to import.

        Example
        -------
        >>> import_dict = [
        >>>     ['season_0', 'q_s', HQ-EX Export1'],
        >>>     ['season_0', 't_A', 'HQ-EX Export2'],
        >>>     ['season_0', 'm_an', 'HQ-EX Export3'],
        >>>     ['season_0', 'm_ab', 'HQ-EX Export4'],
        >>>
        >>>     ['season_1', 'q_s', 'HQ-EX Export5'],
        >>>     ['season_1', 't_A', 'HQ-EX Export6'],
        >>>     ['season_1', 'm_an', 'HQ-EX Export7'],
        >>>     ['season_1', 'm_ab', 'HQ-EX Export8'],
        >>>     ['season_1', 't_p', 'HQ-EX Export9']
        >>> ]"""
        for pset in import_dict:
            hqex = Hqex_dist_para()
            hqex.folder = workspace
            hqex.filename = pset[2]
            hqex.read_stx_file()
            self._dist_fist2[pset[0]]['p'][pset[1]] = hqex # NEW

        self.stx_dist = self._dist_fist2


    def parameter_statistics(self):
        """Generate statistics for the parameters."""
        self.parameter_stats = {}
        for seas_key, seas_val in self.parameter_season.items():
            self.parameter_stats.update({seas_key: {}})
            if seas_key == 'modelname':
                continue
            for par_key, par_val in seas_val['p'].items():
                self.parameter_stats[seas_key].update({par_key: {}})
                self.parameter_stats[seas_key][par_key]['median'] \
                    = np.median(self.parameter_season[seas_key]['p'][par_key])
                self.parameter_stats[seas_key][par_key]['mean'] \
                    = np.mean(self.parameter_season[seas_key]['p'][par_key])
                self.parameter_stats[seas_key][par_key]['std'] \
                    = np.std(self.parameter_season[seas_key]['p'][par_key])
                self.parameter_stats[seas_key][par_key]['min'] \
                    = np.min(self.parameter_season[seas_key]['p'][par_key])
                self.parameter_stats[seas_key][par_key]['max'] \
                    = np.max(self.parameter_season[seas_key]['p'][par_key])
                self.parameter_stats[seas_key][par_key]['number'] \
                    = len(self.parameter_season[seas_key]['p'][par_key])
        return self.parameter_stats


    def show_parameter(self):
        """Print the parameters for the selected fitted distribution."""
        print(self.__selected_parameter)


    # def draw_hqex_dist(self):
    #     """Plot a histogram and density for a distribution."""
    #     fig, ax = plt.subplots(1, 1)

    #     r = self.__dist.rvs(self.__selected_parameter[DIST_MAP_PAR][dist][0],
    #                         loc=self.__selected_parameter[DIST_MAP_PAR][dist][1],
    #                         scale=self.__selected_parameter[DIST_MAP_PAR][dist][2], size=1000)
    #     x = np.linspace(self.__dist.ppf(0.0001, self.__selected_parameter[DIST_MAP_PAR][dist][0]),
    #                     self.__dist.ppf(0.99, self.__selected_parameter[DIST_MAP_PAR][dist][0]), 100)
    #     ax.plot(x, self.__dist.pdf(x, self.__selected_parameter[DIST_MAP_PAR][dist][0]),
    #             'r-', lw=5, alpha=0.6, label='pearson3 pdf')
    #     ax.hist(r, density=True, histtype='stepfilled', alpha=0.2)
    #     ax.legend(loc='best', frameon=False)
    #     plt.show()


    # def draw_par_pdf(self, distribution, estimator):
    #     """Plot a histogram and density for the distributions of all model parameters."""
    #     for seas in self.parameter_season:
    #         fig, ax = plt.subplots(2, 3)
    #         ax = ax.flatten()
    #         p1 = TB_MODELMAP[self.parameter_season[seas]['modelname']]['parameter']
    #         p2 = TB_MODELMAP[self.parameter_season[seas]['modelname']]['parameter_fixed']
    #         parameternames = p1 + p2
    #         param = self.parameter_season[seas]['p']
    #         for j, parameter in enumerate(parameternames):
    #             x = np.linspace(np.min(param[parameter]),
    #                             np.max(param[parameter]), 100)

    #             fpar = self.modelvar_dist[seas]['p'][parameter]
    #             y = DISTF_MAP[distr](x, *self._dist_fist[seas]['p'][parameter].df_prx.loc[(distribution, estimator),
    #                                                                                       DIST_MAP_PARN[distr]].values.tolist() )
    #             ax[j].plot(x, y,
    #                        'r-', lw=5, alpha=0.6,
    #                        label='pearson3 pdf')
    #             ax[j].hist(param[parameter], density=True, histtype='stepfilled',
    #                        alpha=0.5, label=parameternames[j])
    #             ax[j].legend(loc='best', frameon=False)

    #             plt.suptitle('Distribution fitted parameter for {}'.format(seas))
    #             plt.tight_layout()


    # def draw_par_cdf(self, distribution, estimator):
    #     """Plot cumulative density function for the distributions of all model parameters."""
    #     for seas in self.parameter_season:
    #         fig, ax = plt.subplots(2, 3)
    #         ax = ax.flatten()
    #         p1 = TB_MODELMAP[self.parameter_season[seas]['modelname']]['parameter']
    #         p2 = TB_MODELMAP[self.parameter_season[seas]['modelname']]['parameter_fixed']
    #         parameternames = p1 + p2
    #         param = self.parameter_season[seas]['p']
    #         for j, parameter in enumerate(parameternames):
    #             x = np.linspace(np.min(param[parameter]),
    #                             np.max(param[parameter]), 100)
    #             fpar = self.modelvar_dist[seas]['p'][parameter]
    #             y = DIST_MAP[distr](x, *self._dist_fist[seas]['p'][parameter].df_prx.loc[(distribution, estimator),
    #                                                                                      DIST_MAP_PARN[distr]].values.tolist() )
    #             ax[j].plot(x, y,
    #                        'r-', lw=5, alpha=0.6, label='pearson3 pdf')
    #             ax[j].hist(param[parameter], density=True, histtype='stepfilled',
    #                        alpha=0.5, label=parameternames[j], cumulative=True)
    #             ax[j].legend(loc='best', frameon=False)

    #             plt.suptitle('Distribution fitted parameter for {}'.format(seas))
    #             plt.tight_layout()


    def read_Qyear(self, workspace, filex):
        """In case that there are different data bases for yearly high floods and time series."""
        with open(os.path.join(workspace, filex)) as fid:
            tmp = []
            for line in fid:
                try:
                    tmp.append(float(line.strip()))
                except:
                    pass
            self.hqt = np.array(tmp)
            print('done.')


    def return_period_charts(self, parameter, season='season_0', distribution=None, estimator=None, figure=None):
        """Plot the plotting postitions of the parameters against return period for all distributions.

        Parameters
        ----------
        parameter : str
            the parameter name
        season : str
            the current season
        distribution : str
            the """
        resolution = 100
        LS = {'MM': '-', 'MLM': '--', 'WGM': ':'}
        fig = make_subplots(rows=1, cols=1)
        if parameter == 'q_s':
            param = self.hqt
            if not len(self.hqt):
                print('ptx-file for Q_t not read yet, use the read_Qyear method first')
        else:
            param = self.parameter_season[season.lower()]['p']
            param = param[parameter]


        qs = np.flip(np.sort(param))
        mx = qs[0] * 1.5
        qi = (np.arange(len(qs))+1) / (len(qs)+1)  # plotting position -> exceedance
        pi = 1-qi   # non exceedance
        retper = 1/(1-pi)

        if estimator is None:
            estl = ESTIMATORS
        else:
            estl = [estimator]

        if distribution is None:
            dstl = DISTF_MAP
        else:
            dstl = [distribution]

        fig.add_trace(go.Scatter(x=retper, y=qs, name=parameter, mode="markers"),
                      row=1, col=1)
        legtext = []
        for distr in dstl:
            vvec = np.zeros((3, resolution))
            for est in estl:
                try:
                    for i, qx in enumerate(np.linspace(0.1, mx, resolution)):
                        vvec[0, i] = DISTF_MAP[distr](qx, *self._dist_fist[season.lower()]['p'][parameter].df_prx.loc[(distr, est),
                                                       DIST_MAP_PARN[distr]].values.tolist() )
                        vvec[1, i] = qx
                        vvec[2, i] = 1/(1-vvec[0, i])

                    fig.add_trace(go.Scatter(x=vvec[2, :], y=vvec[1, :],
                                             mode="lines", name=distr + ' ' + est),
                                  row=1, col=1)
                except:
                    pass

        rv = [2, 5, 10, 20, 25, 50, 100, 200, 500, 1000]
        fig.update_layout(
            title="Wiederkehrsintervall",
            xaxis_title="T (a)",
            yaxis_title=parameter,
            xaxis=dict(range=[-0.0457, 3]),
            font=dict(
                family="Courier New, monospace",
                size=12
            )
        )

        fig.update_xaxes(type="log")
        if figure is None:
            fig.show()
        else:
            return fig.to_html(include_plotlyjs = 'cdn')
            
