# -*- coding: utf-8 -*-
"""
Bender and Jensen flood event models with different numbers of free parameters.

BenderJensen_fixed_noTp: q_s, m_an, m_ab, t_A, t_p = 0
BenderJensen_fixed_tA: q_s, m_an, m_ab, t_p
BenderJensen_full:q_s, m_an, m_ab, t_A, t_p

Created on Sun May 30 18:18:00 2021
@author: ruben.mueller
"""

import scipy
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from hwlhw.hwlhw_settings import TB_MODELMAP, FFTYPES, TIMESTEP



class BenderJensen_base:
    """Base class for the Bender and Jensen models."""
    def __init__(self):
        self.name = ""
        self.modelname = ""
        self.fitnesstype = ""
        self.fitnesstypes = FFTYPES
        self.__numpar = 0
        self.__guess = []
        self.__boundaries = ((0, 1))
        self.b = 1
        self.a = 1

    def runrun(self, param, event, observed, plot=False, opt=True, ax=None):
        raise NotImplementedError()

    @property
    def boundaries(self):
        return self.__boundaries

    @boundaries.setter
    def boundaries(self, lbound, ubound=None):
        if ubound:
            if not len(lbound) == len(ubound):
                raise ValueError('lbound and ubound must have the same length.')
            if any((x >= y for x, y in zip(lbound, ubound))):
                raise ValueError('Lower boundary greater or equal to the upper boundary detected.')
            self.__boundaries = [lbound, ubound]
        else:
            self.__boundaries = lbound


    @property
    def first_guess(self):
        return self.__guess

    @first_guess.setter
    def first_guess(self, first_guess):
        self.__guess = first_guess


    def _bender_jensen_part_1(self, t_range, event, t_A, q_s, m_an):
        """Calculate the ascending part of the flood event."""
        ts_part1 = t_range[:t_A]
        q_part1 = (q_s - event["Qbase"]) * ((ts_part1/t_A)**m_an * np.exp(m_an * (1 - ts_part1/t_A))) + event["Qbase"] # OK
        return q_part1


    def _bender_jensen_part_3(self, t_range, t_A, q_s, t_p, q_o, m_ab):
        """Calculate the descending part of the flood event."""
        ts_part3 = np.arange(len(t_range)-t_p-t_A)  * 15/60/24  #* TIMESTEP
        # equation (4)
        tmp = np.log(2 * q_o/q_s - 1)
        ts = 1 / (2/tmp - m_ab)
        # equation (3)
        b = self.b
        k = (ts + ts_part3) / (b + m_ab * (ts + ts_part3))
        # # equation (2)
        a = self.a
        upper = np.exp(k)**a - np.exp(-k)**a
        lower = np.exp(k)**a + np.exp(-k)**a
        q_part3 = q_o * (1 - upper/lower)
        return q_part3


    def _bender_jensen_pd(self, data, event):
        """Create a DataFrame for the fitted event."""
        df = pd.DataFrame(data,
            index=pd.date_range(start=event['start'],
            end=event['start'] + datetime.timedelta(minutes=len(data)*15-1),
            freq=event['freq']))
        return df


    def _bender_jensen_plot(self, tsnew, observed, event, ax):
        """Plot the fitted event."""
        if ax is None:
            fig, ax = plt.subplots(1, 1)

        tsnew.plot(ax=ax, label=r"$\mathrm{HQ_{sim}}$")

        if not observed is None:
            observed.loc[event["start"]:event["end"], "Q"].plot(ax=ax, label=r"$\mathrm{HQ_{beob}}$")
            plt.title('TS_{}_Q_{}'.format(datetime.datetime.strftime(event["ts"], "%Y-%m-%dT-%H:%M"), event["Qs"])   )
            plt.xlabel("Datum")
            plt.ylabel(r"$\mathrm{Q}\quad (\mathrm{m^{3}s^{-1}})$")




class BenderJensen_fixed_noTp(BenderJensen_base):
    '''Class for BenderJensen with fixed t_A and zero t_P.'''

    def __init__(self):
        '''Bender and Jensen model with fixed t_A and t_P = 0.
        Parameters: [q_s, m_an, m_ab, t_A]'''
        super().__init__()
        self.name = "BenderJensen_fixed_noTp"
        self.modelname = self.name
        self.fitnesstype = "RMSE"
        self.fitnesstypes = FFTYPES
        self.__numpar = 4
        self.parts = [1, 1, 2, 1]
        self.first_guess = [5, 5, 0.2, 24]
        self.parameternames = TB_MODELMAP[self.name]['parameter']
        self.boundaries = [[0.001, 20000], [1, 10], [0.02, 0.5], [1, 200]]

    def run(self, param, event, observed, plot=False, opt=True, ax=None, part=0):
        """Run the model to create an synthetic flood event.

        Parameters
        ----------
        param : list
            List with the model parameters [q_s, m_an, m_ab].
        event : dict
            Dictionary describing the event ot be fitted.
        observed : pandas.DateFrame
            The observed time series.
        plot : bool, optional
            Plot the synthetic flood event. The default is False.
        opt : bool, optional
            If True, return only the fittness critiera, otherwise a dict with more info. The default is True.
        ax : matplotlib.axis, optional
            plot to this axis. The default is None.

        Returns
        -------
        result : dict or float
            If opt is False a dict with more info is returned.
            If opt is True the fittness criteria is returned.
        """
        if plot and ax is None:
            plt.close()
        try:
            if part == 0:
                q_s = param[0]    
                m_an = param[1] 
                m_ab = param[2]  
                t_A = int(param[3]) 
            else:
                q_s = param[0]      if part in (1, 0) else self.first_guess[0]
                m_an = param[1]     if part in (1, 0) else self.first_guess[1]
                m_ab = param[0]     if part in (2, 0) else self.first_guess[2]
                t_A = int(param[2]) if part in (1, 0) else self.first_guess[3]
            q_o = event["qo"]
            t_p = 0
            q_s = np.min((q_s, q_o-q_o*0.2))

            if not observed is None:
                t_range = np.arange(len(observed.loc[event["start"]:event["end"], "Q"]))
            else:
                t_range = np.arange((((event["end"] - event["start"])).total_seconds()) / event['period'])
            
            q_part1 = self._bender_jensen_part_1(t_range, event, t_A, q_s, m_an)
            q_part3 = self._bender_jensen_part_3(t_range, t_A, q_s, t_p, q_o, m_ab)
            
            data = np.append(q_part1, q_part3)
            tsnew = self._bender_jensen_pd(data, event)

            cutoff = np.argwhere(tsnew.values[t_A+t_p:] < 0.0001)
            if len(cutoff): tsnew.iloc[cutoff[0][0]+t_A+t_p:] = 0
            if plot:
                self._bender_jensen_plot(tsnew, observed, event, ax=ax)

            r = 0
            if not observed is None:
                o = observed.loc[tsnew.index, "Q"].values
                s = tsnew.values
                if part == 1:
                    o = o[:len(q_part1)]
                    s = s[:len(q_part1)]
                elif part == 2:
                    o = o[len(q_part1):len(q_part3)+len(q_part1)]
                    s = s[len(q_part1):len(q_part3)+len(q_part1),:]                    
                r = self.fitnesstypes[self.fitnesstype](o, s)
            if opt:
               result = 9999 if np.isnan(r) else r
            else:
                if not observed is None:
                    if part == 0:
                        vol_obs = scipy.integrate.trapz(observed.loc[tsnew.index[0]:tsnew.index[-1], "Q"])
                        vol_obs = 0
                else:
                    vol_obs = 0
                result = {"vol_obs": vol_obs,
                          "vol_sim": scipy.integrate.trapz(tsnew.values, axis=0)[0] if part == 0 else 0,
                          "fitness": r,
                          "parameter": param,
                          'parameter_fixed': [],
                          "timeseries": tsnew}
            return result
        except:
            return 9999



class BenderJensen_fixed_tA(BenderJensen_base):
    '''Bender and Jensen model with fixed t_A and variable t_P.

    Parameters: [q_s, m_an, m_ab, t_p]'''

    def __init__(self):
        '''Bender and Jensen model with fixed t_A variable t_P.
        Parameters: [q_s, m_an, m_ab, t_p]'''
        super().__init__()
        self.name = "BenderJensen_fixed_tA"
        self.modelname = self.name
        self.fitnesstype = "RMSE"
        self.fitnesstypes = FFTYPES
        self.__numpar = 4
        self.first_guess = [5, 5, 0.2, 4]
        self.parameternames = TB_MODELMAP[self.name]['parameter']
        self.boundaries = [[0.001, 20000], [1, 10], [0.02, 0.5], [1, 192]]

    def run(self, param, event, observed, plot=False, opt=True, ax=None):
        """Run the model to create an synthetic flood event.

        Parameters
        ----------
        param : list
            List with the model parameters [q_s, m_an, m_ab, t_p].
        event : dict
            Dictionary describing the event ot be fitted.
        observed : pandas.DateFrame
            The observed time series.
        plot : bool, optional
            Plot the synthetic flood event. The default is False.
        opt : bool, optional
            If True, return only the fittness critiera, otherwise a dict with more info. The default is True.
        ax : matplotlib.axis, optional
            plot to this axis. The default is None.

        Returns
        -------
        result : dict or float
            If opt is False a dict with more info is returned.
            If opt is True the fittness criteria is returned.
        """
        if plot:
            plt.close()
        try:
            q_s = param[0]
            m_an = param[1]
            m_ab = param[2]
            t_p = int(param[3])
            q_o = event["qo"]
            t_A =  int(event["ta"].total_seconds()/event['period']) \
                   if isinstance(event["ta"], datetime.timedelta) else np.max((int(event["ta"]), 1))
            q_s = np.min((q_s, q_o-q_o*0.2))

            if not observed is None:
                t_range = np.arange(len(observed.loc[event["start"]:event["end"], "Q"]))
            else:
                t_range = np.arange((((event["end"] - event["start"])).total_seconds()) / event['period'])
            q_part1 = self._bender_jensen_part_1(t_range, event, t_A, q_s, m_an)

            q_part3 = self._bender_jensen_part_3(t_range, t_A, q_s, t_p, q_o, m_ab)
            
            q_part2 = np.ones(t_p) * q_part3[0]

            data = np.append(q_part1, q_part2)
            data = np.append(data, q_part3)
            tsnew = self._bender_jensen_pd(data, event)
            cutoff = np.argwhere(tsnew.values[t_A+t_p:] < 0.0001)
            if len(cutoff): tsnew.iloc[cutoff[0][0]+t_A+t_p:] = 0
            if plot:
                self._bender_jensen_plot(tsnew, observed, event, ax=ax)

            r = 0
            if not observed is None:
                r = self.fitnesstypes[self.fitnesstype](observed.loc[tsnew.index, "Q"].values, tsnew.values)
            if opt:
                result = 9999 if np.isnan(r) else r
            else:
                if not observed is None:
                    vol_obs = scipy.integrate.trapz(observed.loc[tsnew.index[0]:tsnew.index[-1], "Q"])
                else:
                    vol_obs = 0
                result = {"vol_obs": vol_obs,
                          "vol_sim": scipy.integrate.trapz(tsnew.values[:, 0]),
                          "fitness": r,
                          "parameter": param,
                          'parameter_fixed': [t_A],
                          "timeseries": tsnew}
            return result
        except:
            return 9999


class BenderJensen_form(BenderJensen_base):
    '''Bender and Jensen model with fixed t_A and variable t_P.

    Parameters: [q_s, m_an, m_ab, t_p]'''

    def __init__(self):
        '''Bender and Jensen model with fixed t_A variable t_P.
        Parameters: [q_s, m_an, m_ab, t_p]'''
        super().__init__()
        self.name = "BenderJensen_form"
        self.modelname = self.name
        self.fitnesstype = "RMSE"
        self.fitnesstypes = FFTYPES
        self.__numpar = 2
        self.first_guess = [2, 0.3]
        self.parameternames = TB_MODELMAP[self.name]['parameter']
        self.boundaries = [[1.5, 10], [0.02, 0.5]]

    def run(self, param, event, observed, plot=False, opt=True, ax=None):
        """Run the model to create an synthetic flood event.

        Parameters
        ----------
        param : list
            List with the model parameters [q_s, m_an, m_ab, t_p].
        event : dict
            Dictionary describing the event ot be fitted.
        observed : pandas.DateFrame
            The observed time series.
        plot : bool, optional
            Plot the synthetic flood event. The default is False.
        opt : bool, optional
            If True, return only the fittness critiera, otherwise a dict with more info. The default is True.
        ax : matplotlib.axis, optional
            plot to this axis. The default is None.

        Returns
        -------
        result : dict or float
            If opt is False a dict with more info is returned.
            If opt is True the fittness criteria is returned.
        """
        if plot:
            plt.close()
        try:
            q_s = event["Qs"]
            m_an = param[0]
            m_ab = param[1]
            t_p = 0
            q_o = event["qo"]
            t_A =  int(event["ta"].total_seconds()/event['period']) \
                   if isinstance(event["ta"], datetime.timedelta) else np.max((int(event["ta"]), 1))
            q_s = np.min((q_s, q_o-q_o*0.2))

            if not observed is None:
                t_range = np.arange(len(observed.loc[event["start"]:event["end"], "Q"]))
            else:
                t_range = np.arange((((event["end"] - event["start"])).total_seconds()) / event['period'])
            q_part1 = self._bender_jensen_part_1(t_range, event, t_A, q_s, m_an)

            q_part3 = self._bender_jensen_part_3(t_range, t_A, q_s, t_p, q_o, m_ab)
            
            q_part2 = np.ones(t_p) * q_part3[0]

            data = np.append(q_part1, q_part2)
            data = np.append(data, q_part3)
            tsnew = self._bender_jensen_pd(data, event)
            cutoff = np.argwhere(tsnew.values[t_A+t_p:] < 0.0001)
            if len(cutoff): tsnew.iloc[cutoff[0][0]+t_A+t_p:] = 0
            if plot:
                self._bender_jensen_plot(tsnew, observed, event, ax=ax)

            r = 0
            if not observed is None:
                r = self.fitnesstypes[self.fitnesstype](observed.loc[tsnew.index, "Q"].values, tsnew.values)
            if opt:
                result = 9999 if np.isnan(r) else r
            else:
                if not observed is None:
                    vol_obs = scipy.integrate.trapz(observed.loc[tsnew.index[0]:tsnew.index[-1], "Q"])
                else:
                    vol_obs = 0
                result = {"vol_obs": vol_obs,
                          "vol_sim": scipy.integrate.trapz(tsnew.values[:, 0]),
                          "fitness": r,
                          "a": self.a,
                          "b": self.b,
                          "parameter": param,
                          'parameter_fixed': [t_A, q_s],
                          "timeseries": tsnew}
            return result
        except:
            return 9999


class BenderJensen_full(BenderJensen_base):
    '''Bender and Jensen model with full parameter set.

    Parameters: [q_s, t_A, m_an, m_ab, t_p]'''

    def __init__(self):
        '''Bender and Jensen model with the full parameter set.
        Parameters: [q_s, t_A, m_an, m_ab, t_p]'''
        super().__init__()
        self.name = "BenderJensen_full"
        self.modelname = self.name
        self.fitnesstype = "RMSE"#"KlingGuptaEfficiency"
        self.fitnesstypes = FFTYPES
        self.__numpar = 5
        self.first_guess = [3, 3, 2, 0.5, 2]
        self.parameternames = TB_MODELMAP[self.name]['parameter']
        self.boundaries = [[0.001, 20000], [1, 192], [1, 10], [0.02, 0.5], [1, 192]]

    def run(self, param, event, observed, plot=False, opt=True, ax=None):
        """Run the model to create an synthetic flood event.

        Parameters
        ----------
        param : list
            List with the model parameters [q_s, t_A, m_an, m_ab, t_p].
        event : dict
            Dictionary describing the event ot be fitted.
        observed : pandas.DateFrame
            The observed time series.
        plot : bool, optional
            Plot the synthetic flood event. The default is False.
        opt : bool, optional
            If True, return only the fittness critiera, otherwise a dict with more info. The default is True.
        ax : matplotlib.axis, optional
            plot to this axis. The default is None.

        Returns
        -------
        result : dict or float
            If opt is False a dict with more info is returned.
            If opt is True the fittness criteria is returned.
        """
        if plot:
            plt.close()
        try:
            q_s = param[0]
            t_A = int(param[1])
            m_an = param[2]
            m_ab = param[3]
            t_p = int(param[4])
            q_o = event["qo"]
            q_s = np.min((q_s, q_o*0.8))

            if not observed is None:
                t_range = np.arange(len(observed.loc[event["start"]:event["end"], "Q"])) #* 15/60/24
            else:
                t_range = np.arange((((event["end"] - event["start"])).total_seconds()) / event['period']) #* 15/60/24
            q_part1 = self._bender_jensen_part_1(t_range, event, t_A, q_s, m_an)

            q_part3 = self._bender_jensen_part_3(t_range, t_A, q_s, t_p, q_o, m_ab)
            
            q_part2 = np.ones(t_p) * q_part3[0]

            data = np.append(q_part1, q_part2)
            data = np.append(data, q_part3)
            tsnew =  self._bender_jensen_pd(data, event)
            cutoff = np.argwhere(tsnew.values[t_A+t_p:] < 0.0001)
            if len(cutoff): tsnew.iloc[cutoff[0][0]+t_A+t_p:] = 0
            if plot:
                self._bender_jensen_plot(tsnew, observed, event, ax=ax)

            r = 0
            if not observed is None:
                r = self.fitnesstypes[self.fitnesstype](observed.loc[tsnew.index, "Q"].values, tsnew.values)
            #print(r)
            if opt:
                result = 9999 if np.isnan(r) else r
            else:
                if not observed is None:
                    vol_obs = scipy.integrate.trapz(observed.loc[tsnew.index[0]:tsnew.index[-1], "Q"])
                else:
                    vol_obs = 0

                result = {"vol_obs": vol_obs,
                          "vol_sim": scipy.integrate.trapz(tsnew.values[:, 0]),
                          "fitness": r,
                          "parameter": param,
                          'parameter_fixed': [],
                          "timeseries": tsnew}
            return result
        except:
            return 9999

TB_MODELMAP['BenderJensen_fixed_noTp']['model'] = BenderJensen_fixed_noTp
TB_MODELMAP['BenderJensen_fixed_tA']['model'] = BenderJensen_fixed_tA
TB_MODELMAP['BenderJensen_full']['model'] = BenderJensen_full
TB_MODELMAP['BenderJensen_form']['model'] = BenderJensen_form
