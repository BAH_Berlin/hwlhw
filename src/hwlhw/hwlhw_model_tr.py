# -*- coding: utf-8 -*-
"""
Simple trianlge flood event model for debugging.
Usage is unsupported.

Created on Sun May 30 18:10:50 2021
@author: ruben.mueller
"""

# import datetime
# import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt
# from hwlhw.hwlhw_criteria import KlingGuptaEfficiency

# class Triangle_rmse:
#     """Simple triangle 'linear' flood event model."""
#     def __init__(self):
#         self.name = "triangle_rmse"
#         self.modelname = "trianlge"
#         self.fitnesstype = "RMSE"
#         self.numpar = 4
#         self.__guess = [25, 60, 0.31, 0.11]
#         self.__boundaries = ((10, 70), (10, 70), (0.001, 10), (0.001, 10))

#     def run(self, param, event, observed, plot=False) -> np.float:
#         """Run the model.

#         Parameters
#         ----------
#         param : list
#             The model parameters [uptime, downtime, slope_up, slope_down].
#         event : dict
#             Dictionary describing the event ot be fitted.
#         observed : pandas.DateFrame
#             The observed time series.
#         plot : bool, optional
#             Set to True to plot the result. The default is False.

#         Returns
#         -------
#         r : float
#             the fitting criteria.

#         """
#         if plot:
#             plt.close()
#         t_a = param[0]
#         m_ab = param[1]
#         t_p = param[2]
#         m_ab = param[3]
#         m_an = np.arange(0, t_a)
#         down = np.arange(0, t_p)
#         a = up * m_an + event["event"]["Qbase"]
#         b = down * -m_ab + a[-1]

#         tss = event["event"]['ts'] - datetime.timedelta(hours=uptime)
#         tse = event["event"]['ts'] + datetime.timedelta(hours=downtime - 1)
#         tsnew = pd.DataFrame(np.append(a, b),
#                              index=pd.date_range(start=tss,
#                                                  end=tse, freq='H'))
#         tsnew = tsnew.clip(event["Qbase"])
#         if plot:
#             fig, ax = plt.subplots(1, 1)
#             tsnew.plot(ax=ax, label="Qsim")
#             observed.timeseries.series_gauge.loc[tss:tse, "Q"].plot(ax=ax, label="Q")

#         r = rmse(observed.loc[tss:tse, "Q"].values, tsnew.values.T)
#         return r

#     @property
#     def boundaries(self):
#         return self.__boundaries

#     @boundaries.setter
#     def boundaries(self, lbound, ubound):
#         if not len(lbound) == len(ubound):
#             raise ValueError('lbound and ubound must have the same length.')
#         if any(x >= y for x, y in zip(lbound, ubound)):
#             raise ValueError('Lower boundary greater or equal to the upper boundary detected.')
#         self.__boundaries = [lbound, ubound]

#     @property
#     def first_guess(self):
#         return self.__guess

#     @first_guess.setter
#     def first_guess(self, first_guess):
#         self.__guess = first_guess
