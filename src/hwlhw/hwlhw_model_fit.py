# -*- coding: utf-8 -*-
"""
Fit flood event models to all separated flood events.
Standard optimization method for parameter fitting is the dual annealing
optimizer from scipy.

Warnings are disabledfor now because certain parameter combinations trigger
overflow conditions. This does not happen often, but nevertheless clutters the output
of the jupyter notebook.

Created on Sun May 30 18:41:05 2021
@author: ruben.mueller
"""

import os
import scipy
import json
import pickle
import datetime
import pandas as pd
import numpy as np
from multiprocessing import Pool, cpu_count
from hwlhw.hwlhw_model_tb import TB_MODELMAP
from hwlhw.hwlhw_model_tb import (BenderJensen_fixed_noTp,
                                  BenderJensen_fixed_tA,
                                  BenderJensen_full)
from hwlhw.hwlhw_separate_events import Separate_events
from hwlhw.hwlhw_utils import update_progress

import warnings
warnings.filterwarnings('ignore')

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()
    elif isinstance(o, (BenderJensen_fixed_noTp,
                        BenderJensen_fixed_tA,
                        BenderJensen_full)):
        return o.name
    elif isinstance(o, pd.core.frame.DataFrame):
        return o.to_json(date_format='iso', orient='columns')
    elif isinstance(o, np.ndarray):
        return o.tolist()
    elif isinstance(o, pd._libs.tslibs.timedeltas.Timedelta):
        return '{} hours'.format(o.total_seconds()/3600)


class Model_fitter():
    """Class for fitting the models to events."""


    def __init__(self, s_ev, models):
        """Fit the synthetic flow model for events.

        Parameters
        ----------
        s_ev : Separate_Events
            Class for event separation.
        models : list
            List with a model name for each season."""

        self.models = {}
        self.models_name = models
        self.s_ev = s_ev

        if isinstance(models, str):
            self._check_models(models, 0)
            if not  len(s_ev.seasons) == 1:
                raise ValueError('Mismatch! Check models given here and seasons in "Separate_events"')
        elif isinstance(models, (list, tuple)):
            if not len(models) == len(s_ev.seasons):
                raise ValueError('Mismatch! Check models given here and seasons in "Separate_events"')
            for i, m in enumerate(models):
                self._check_models(m, i)
        elif isinstance(models, dict):
            for m in models.items():
                self._check_models(m[1], m[0])

            # check for models and season match
            s_ev_check = [i for i, x in enumerate(s_ev.seasons)]
            if any((True for x in models if not x in s_ev_check)) \
            or any((True for x in s_ev_check if not x in models)):
                raise ValueError('Mismatching seasons! Check seasons given here and in "Separate_events"')


    def _check_models(self, x, s):
        if x in TB_MODELMAP:
            self.models.update({s: TB_MODELMAP[x]['model']()})
        else:
            raise ValueError('Model %s is unknown.' % x)


    def fit_events(self, signal=None):
        """Fit the models for all events in the event list sequentially."""
        self._optimize_serial(signal=signal)


    def set_boundary(self, season, boundary):
        """Set the boundary for one model

        Parameters
        ----------
        season : int, str
            | set the boundary for a model with this season
            | int - 0 or 1 for Season_0 or Season_1
            | str - 'Season_0' or 'Season_1'
        boundary : list
            the upper and lower boundaries for the parameters"""

        if isinstance(season, str):
            season = int(season[-1])
        self.models[season].boundaries = boundary


    def set_a_b(self, season, a=1, b=1):
        if isinstance(season, str):
            season = int(season[-1])
        self.models[season].a = a
        self.models[season].b = b


    def optimize_single_event(self, event_name):
        """Optimize a single event"""
        event = self.s_ev.events[event_name]
        model = self.models[self.s_ev.events[event_name]['season']]
        #model.boundaries[0][1] =  self.s_ev.timeseries.qo
        res = scipy.optimize.dual_annealing(model.run,
                                            x0=model.first_guess,
                                  bounds=model.boundaries,
                                  args=(event,
                                        self.s_ev.timeseries.series_gauge, False))
        vols = model.run(res.x, event,
                         self.s_ev.timeseries.series_gauge, False, False)
        self.s_ev.events[event_name].update(vols)

        self.s_ev.events[event_name].update({'model': model.name,
                                             'fitnesstype':model.fitnesstype,
                                             'season': self.s_ev.events[event_name]['season'],
                                             'modelname': self.models[event['season']].name})


    def _optimize_serial(self, signal=None):
        lev = len(self.s_ev.events)
        
        if signal is None:
            updater = update_progress
        else:
            updater = signal.emit
        updater([0.0, '...'])
        for i, event_name in enumerate(self.s_ev.events):
            self.optimize_single_event(event_name)
            tmp = [float(i+1)/float(lev),
                            'Season {}, event {}'.format(self.s_ev.events[event_name]['season'],
                                                          event_name)]
            updater(tmp)


    def fit_events_parallel(self, ts_q,  cores=None):
        """Fit the models for all events in the event list parallized.
        Does not work in a Jupyter notebook!

        Parameters
        ----------
        ts_q : Timeseries_gauge
            Class with the flow time series.
        cores : int, optional
            The number of CPU cores to utulize. None uses all available cores. The default is None."""
        all_args = [(self.s_ev, event) for event in self.s_ev.events]
        cores = cpu_count() if cores is None else cores
        pool = Pool(cores)
        results = pool.map(self._executor, all_args)
        pool.close()
        pool.join()

        for event in results:
            self.s_ev.events[event[0]].update(event[1])
            self.s_ev.events[event[0]]['modelname'] = self.models[event[1]['season']].name


    def _executor(self, args):
        """Helper method for the parellized model fit."""
        event = args[0].events[args[1]]
        model = self.models[event['season']]
        #model.boundaries[0][1] = args[0].timeseries.qo
        res = scipy.optimize.dual_annealing(model.run, x0=model.first_guess,
                                            bounds=model.boundaries,
                                            args=(event,
                                                  args[0].timeseries.series_gauge,
                                                  False))
        vols = model.run(res.x, event, args[0].timeseries.series_gauge, False, False)
        vols.update({'model': model,
                     'fitnesstype':model.fitnesstype,
                     'season': event['season']})
        return args[1], vols


    def write_HXEQ_input(self, workspace, rivername, gaugename):
        """Write import files for HQ-EX.

        Parameters
        ----------
        workspace : str
            the workspace folder.
        rivername : str
            the river name.
        gaugename : str
            the gauge name."""
        for s, _ in enumerate(self.s_ev.seasons):
            for event in self.s_ev.events.items():
                if event[1]["season"] == s:
                    p1 = TB_MODELMAP[event[1]['modelname']]['parameter']
                    p2 = TB_MODELMAP[event[1]['modelname']]['parameter_fixed']
                    parameternames = p1 + p2
                    for p in parameternames:
                        pfile = os.path.join(workspace,
                                             'HQ-EX_Import_{}_{}.hqx'.format(s, p))
                        with open(pfile, 'w') as fid:
                            fid.write('%s\n' % rivername)
                            fid.write('%s\n' % gaugename)
                            fid.write('Reihe: Jahr\n')
                            fid.write('{} m³/s 71.5\n'.format(self.s_ev.timeseries.series_gauge.index[0].year))

            for event in self.s_ev.events.items():
                if event[1]["season"] == s:
                    p1 = TB_MODELMAP[event[1]['modelname']]['parameter']
                    p2 = TB_MODELMAP[event[1]['modelname']]['parameter_fixed']
                    for p in p1:
                        pfile = os.path.join(workspace, 'HQ-EX_Import_{}_{}.hqx'.format(s, p))
                        with open(pfile, 'a') as fid:
                            fid.write('{}\n'.format(event[1]['parameter'][p1.index(p)]))
                    for p in p2:
                        pfile = os.path.join(workspace, 'HQ-EX_Import_{}_{}.hqx'.format(s, p))
                        with open(pfile, 'a') as fid:
                            fid.write('{}\n'.format(event[1]['parameter_fixed'][p2.index(p)]))


    def write_results_json(self, workspace):
        """Write an json file with an overview into the workspace folder.

        Parameters
        ----------
        workspace : str
            The workspace folder."""
        with open(os.path.join(workspace, 'Events.json'), "w") as fid:

            json.dump([self.s_ev.events],  fid, default=myconverter, indent = 4)


    def save_state(self, workspace):
        with open(os.path.join(workspace, 'Fitted_model_part1.pkl'), 'wb') as fid:
            self.s_ev.save_state(workspace, 'Fitted_model_part0.pkl')
            pickle.dump(self.models_name, fid)

    @staticmethod
    def load_state(workspace, ts_q, file='Fitted_model_part1.pkl'):

        tmp = os.path.join(workspace, 'Fitted_model_part1.pkl')
        if not os.path.isfile(tmp):
            raise FileNotFoundError(tmp + ' not found.')
        tmp = os.path.join(workspace, 'Fitted_model_part0.pkl')
        if not os.path.isfile(tmp):
            raise FileNotFoundError(tmp + ' not found.')

        with open(os.path.join(workspace, 'Fitted_model_part1.pkl'), 'rb') as fid:
            tmp = pickle.load(fid)
            s_ev = Separate_events.load_state(workspace, ts_q, 'Fitted_model_part0.pkl')
            x = Model_fitter(s_ev, tmp)
            return x
