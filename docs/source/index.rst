Welcome to hwlhw's documentation!
=================================

hwlhw is a statistical tool to create synthetic flood events with Monte-Carlo and mean parameter methods. 
The flood event model is from Bender and Jensen (2012).


The software was funded by the Landesbetrieb für Hochwasserschuz und Wasserwirtschaft Sachsen-Anhalt (LHW Sachsen-Anhalt).


Example
-------

Files
~~~~~

 1) User seperated events
 The user seperated events are read from a file with the following layout

 name; comment1; comment2
 Start;End;Alternative_Peak
 04.01.2007 07:00;06.01.2007 07:00;
 22.12.2011 20:00;25.12.2011 11:00;23.12.2011 09:45
 06.01.2007 21:00;08.01.2007 05:00;

 The first line is for comments, the second is the header with the 
 keywords Start, End and Alternative_Peak, where
  * Start is the beginning of an event
  * End is the ending of an event
  * Alternative_Peak gives a fixed date with the peak flow of the event. If no date is given, the peak is selected automatically.




´´´
import hwlhw
import datetime
import os
import scipy
import numpy as np
import pandas as pd
from multiprocessing import freeze_support
import matplotlib.pyplot as plt
´´´


Literature
----------
Bender, Jens and Jensen, Jürgen (2012): Ein erweitertes Verfahren zur Generierung synthetischer Bemessungshochwasserganglinien, WasserWirtschaft, 3/2012, pp. 35-39.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Python API reference <api/hwlhw.rst>
   license.rst

