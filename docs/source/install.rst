Installing base_bah
===================

base_bah should work on Python 3.6 (or later) on Windows, Linux or OS X.


The following command will build and install base_bah:

.. code-block:: shell

  python setup.py install


Installing binary wheels with pip
---------------------------------

Binary wheel distributions of base_bah are hosted on `Pypi <https://bitbucket.org/BAH_Berlin/base_bah>_`.

.. code-block:: shell

  pip install base_bah
