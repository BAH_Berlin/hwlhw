Automatic model parameterization
================================

.. automodule:: hwlhw_model_fit
    :members:
    :undoc-members:
    :show-inheritance:
