Triangle flood event models
===========================

.. automodule:: hwlhw_model_tr
    :members:
    :undoc-members:
    :show-inheritance:
