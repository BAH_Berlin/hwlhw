# hwlhw

Generate synthetic flood events with the model after Bender and Jensen (2012).
hwlhw lets you automatically fit the model to a set of user-separated events and use Monte-Carlo to generate multiple
synthetic flood events.

Bender, Jens and Jensen, Jürgen (2012): Ein erweitertes Verfahren zur Generierung synthetischer Bemessungshochwasserganglinien, WasserWirtschaft, 3/2012, pp. 35-39.

## Author
Ruben Müller<br>
Büro für Angewandte Hydrologie, Berlin  <br>
software@bah-berlin.de<br>
[webpage](www.bah-berlin.de)

![BAH](docs/_static/bah-logo.png)

## License
Copyright (C) 2021 Ruben Müller, Büro für Angewandte Hydrologie, Berlin  

This program is free software; you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation; either version 1, or (at your option)  
any later version.

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License  
along with this program; if not, write to the Free Software  
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA.